package ba.unsa.etf.rma.broadcastreceiveri;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import ba.unsa.etf.rma.R;

public class InternetBroadcastReceiver extends BroadcastReceiver {

    private Aktivnost aktivnost;

    public void setAktivnost(Aktivnost aktivnost) {
        this.aktivnost = aktivnost;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            Toast.makeText(context, R.string.toast_nema_konekcije, Toast.LENGTH_SHORT).show();
            aktivnost.izmijeniStatusKonekcije(false);
            return;
        }
        if (networkInfo.isConnected()) {
            Toast.makeText(context, R.string.toast_ima_konekcije, Toast.LENGTH_SHORT).show();
            aktivnost.izmijeniStatusKonekcije(true);
        }
        else {
            Toast.makeText(context, R.string.toast_nema_konekcije, Toast.LENGTH_SHORT).show();
            aktivnost.izmijeniStatusKonekcije(false);
        }
    }

    public interface Aktivnost {
        void izmijeniStatusKonekcije(boolean imaKonekcije);
    }
}
