package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.KvizoviGridViewAdapter;

public class DetailFrag extends Fragment {

    private OnItemClick onItemClick;
    private ArrayList<Kviz> listaPrikazanihKvizova = new ArrayList<>();
    private KvizoviGridViewAdapter kvizoviGridViewAdapter;
    private GridView gridKvizovi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_frag, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dobaviWidgete();
        if (getArguments().containsKey("bundleListaPrikazanihKvizova")) {
            listaPrikazanihKvizova = getArguments().getParcelableArrayList("bundleListaPrikazanihKvizova");
            pripremiDodavanjeKvizaZaGridView();
            kvizoviGridViewAdapter = new KvizoviGridViewAdapter(getActivity(), listaPrikazanihKvizova, getActivity().getResources());
            gridKvizovi.setAdapter(kvizoviGridViewAdapter);
            try {
                onItemClick = (OnItemClick) getActivity();
            } catch (ClassCastException exception) {
                throw new ClassCastException("Problem sa interfejsom - treba ga implementirati!");
            }
            gridKvizovi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position != listaPrikazanihKvizova.size() - 1) onItemClick.onKvizLongClicked(position);
                    else onItemClick.onKvizClickedDodajKviz();
                    return true;
                }
            });
            gridKvizovi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position != listaPrikazanihKvizova.size() - 1) onItemClick.onKvizClickedIgrajKviz(position);
                }
            });
        }
    }

    private void dobaviWidgete() {
        gridKvizovi = (GridView) getView().findViewById(R.id.gridKvizovi);
    }

    private void pripremiDodavanjeKvizaZaGridView() {
        if (!jeLiVecInicijalizovanGridView()) {
            Kviz kvizFiktivni = new Kviz();
            kvizFiktivni.setNaziv(getResources().getString(R.string.grid_view_item_dodaj_kviz));
            Kategorija kategorijaFiktivnogKviza = new Kategorija();
            kategorijaFiktivnogKviza.setNaziv("Fiktivni");
            kategorijaFiktivnogKviza.setId("671");
            kvizFiktivni.setKategorija(kategorijaFiktivnogKviza);
            listaPrikazanihKvizova.add(kvizFiktivni);
        }
    }

    private boolean jeLiVecInicijalizovanGridView() {
        String nazivFiktivnogKviza = getResources().getString(R.string.grid_view_item_dodaj_kviz);
        for (Kviz kviz : listaPrikazanihKvizova) {
            if (kviz.getNaziv().equals(nazivFiktivnogKviza)) {
                return true;
            }
        }
        return false;
    }

    public interface OnItemClick {
        void onKvizLongClicked(int position);
        void onKvizClickedIgrajKviz(int position);
        void onKvizClickedDodajKviz();
    }
}
