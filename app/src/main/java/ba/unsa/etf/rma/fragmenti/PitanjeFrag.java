package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Pitanje;

public class PitanjeFrag extends Fragment {

    private OnItemClick onItemClick;
    private ZahtijevanjeImenaIgraca zahtijevanjeImenaIgraca;
    private Pitanje trenutnoPitanje;
    private ArrayList<String> listaPrikazanihOdgovora = new ArrayList<>();
    private TextView tekstPitanja;
    private ListView odgovoriPitanja;
    private ArrayAdapter<String> randomOdgovoriAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pitanje_frag, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dobaviWidgete();
        try {
            zahtijevanjeImenaIgraca = (ZahtijevanjeImenaIgraca) getActivity();
        } catch (ClassCastException exception) {
            throw new ClassCastException("Problem sa interfejsom - treba ga implementirati!");
        }
        if (getArguments().containsKey("bundlePitanjeZaOdgovaranje")) {
            trenutnoPitanje = (Pitanje) getArguments().getSerializable("bundlePitanjeZaOdgovaranje");
            listaPrikazanihOdgovora = trenutnoPitanje.dajRandomOdgovore();
            tekstPitanja.setText(trenutnoPitanje.getNaziv());
            randomOdgovoriAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, listaPrikazanihOdgovora);
            odgovoriPitanja.setAdapter(randomOdgovoriAdapter);
            postaviPrilagodjenuVelicinuListeOdgovora(odgovoriPitanja);
            try {
                onItemClick = (OnItemClick) getActivity();
            } catch (ClassCastException exception) {
                throw new ClassCastException("Problem sa interfejsom - treba ga implementirati!");
            }
            odgovoriPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    boolean tacanKliknut = false;
                    int pozicijaTacnogOdgovora = 0;
                    for (int i = 0; i < listaPrikazanihOdgovora.size(); i++) {
                        if (listaPrikazanihOdgovora.get(i).equals(trenutnoPitanje.getTacan())) {
                            pozicijaTacnogOdgovora = i;
                            break;
                        }
                    }
                    if (position == pozicijaTacnogOdgovora) {
                        view.setBackgroundColor(getResources().getColor(R.color.zelena));
                        tacanKliknut = true;
                    }
                    else {
                        view.setBackgroundColor(getResources().getColor(R.color.crvena));
                        odgovoriPitanja.getChildAt(pozicijaTacnogOdgovora).setBackgroundColor(getResources().getColor(R.color.zelena));
                        tacanKliknut = false;
                    }
                    final int pozicija = position;
                    final boolean tacanOdgovorKliknut = tacanKliknut;
                    Runnable nastavak = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                onItemClick.onItemClicked(pozicija, tacanOdgovorKliknut);
                            } catch (Exception greska) {

                            }
                        }
                    };
                    Handler handler = new Handler();
                    handler.postDelayed(nastavak, 2000);
                }
            });
        }
        else {  // Ako nema vise pitanja (ili ako kviz nema nikako pitanja) smatra se da je kviz zavrsen
            tekstPitanja.setText(getResources().getString(R.string.obavijest_kviz_zavrsen));
            zahtijevanjeImenaIgraca.pitajIgracaZaIme();
        }
    }

    private void dobaviWidgete() {
        tekstPitanja = (TextView) getView().findViewById(R.id.tekstPitanja);
        odgovoriPitanja = (ListView) getView().findViewById(R.id.odgovoriPitanja);
    }

    private void postaviPrilagodjenuVelicinuListeOdgovora(ListView listaOdgovora) {
        ArrayAdapter<String> adapterZaOdgovore = (ArrayAdapter<String>) listaOdgovora.getAdapter();
        if (adapterZaOdgovore != null) {
            ViewGroup viewGroup = listaOdgovora;
            int visinaListeOdgovora = 0;
            int brojElemenata = adapterZaOdgovore.getCount();
            for (int pozicija = 0; pozicija < brojElemenata; pozicija++) {
                View elementListeOdgovora = adapterZaOdgovore.getView(pozicija, null, viewGroup);
                elementListeOdgovora.measure(0, 0);
                visinaListeOdgovora += elementListeOdgovora.getMeasuredHeight();
            }
            ViewGroup.LayoutParams parametriLayouta = listaOdgovora.getLayoutParams();
            parametriLayouta.height = visinaListeOdgovora + ((brojElemenata - 1) * listaOdgovora.getDividerHeight());
            listaOdgovora.setLayoutParams(parametriLayouta);
            listaOdgovora.requestLayout();
        }
    }

    public interface OnItemClick {
        void onItemClicked(int position, boolean tacanOdgovorKliknut);
    }

    public interface ZahtijevanjeImenaIgraca {
        void pitajIgracaZaIme();
    }
}
