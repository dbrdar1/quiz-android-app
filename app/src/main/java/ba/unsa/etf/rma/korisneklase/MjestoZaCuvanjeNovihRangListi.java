package ba.unsa.etf.rma.korisneklase;

import java.util.ArrayList;

import ba.unsa.etf.rma.klase.ElementRangListe;
import ba.unsa.etf.rma.klase.RangListaObjekat;

public class MjestoZaCuvanjeNovihRangListi {

    private static ArrayList<RangListaObjekat> listaRangListi = new ArrayList<>();

    public static ArrayList<RangListaObjekat> getListaRangListi() {
        return listaRangListi;
    }

    public static void dodajNovuRangListu(RangListaObjekat rangListaObjekat) {
        listaRangListi.add(rangListaObjekat);
    }

    public static boolean daLiPostojiRangListaSaNazivomKviza(String nazivKviza) {
        return nadjiRangListuPoNazivuKviza(nazivKviza) != null;
    }

    public static void dodajNoviElementURangListu(String nazivKviza, ElementRangListe elementRangListe) {
        RangListaObjekat rangListaObjekat = nadjiRangListuPoNazivuKviza(nazivKviza);
        if (rangListaObjekat != null) {
            rangListaObjekat.dodajNoviElementUListu(elementRangListe);
            rangListaObjekat.sortirajListu();
        }
    }

    public static RangListaObjekat nadjiRangListuPoNazivuKviza(String nazivKviza) {
        for (RangListaObjekat rangListaObjekat : listaRangListi) {
            if (rangListaObjekat.getNazivKviza().equals(nazivKviza)) return rangListaObjekat;
        }
        return null;
    }

    public static void resetujListuRangListi() {
        listaRangListi = new ArrayList<>();
    }
}
