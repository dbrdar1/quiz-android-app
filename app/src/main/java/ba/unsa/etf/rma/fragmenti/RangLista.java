package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class RangLista extends Fragment {

    private ArrayList<String> listaPrikazanihIgraca = new ArrayList<>();
    private ListView lvRangLista;
    private ArrayAdapter<String> rangListaAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rang_lista, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dobaviWidgete();
        if (getArguments().containsKey("bundleRangListaIgraca")) {
            listaPrikazanihIgraca = getArguments().getStringArrayList("bundleRangListaIgraca");
            rangListaAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, listaPrikazanihIgraca);
            lvRangLista.setAdapter(rangListaAdapter);
            postaviPrilagodjenuVelicinuRangListe(lvRangLista);
        }
    }

    private void dobaviWidgete() {
        lvRangLista = (ListView) getView().findViewById(R.id.lvRangLista);
    }

    private void postaviPrilagodjenuVelicinuRangListe(ListView lvRangLista) {
        ArrayAdapter<String> adapterZaIgrace = (ArrayAdapter<String>) lvRangLista.getAdapter();
        if (adapterZaIgrace != null) {
            ViewGroup viewGroup = lvRangLista;
            int visinaListeIgraca = 0;
            int brojElemenata = adapterZaIgrace.getCount();
            for (int pozicija = 0; pozicija < brojElemenata; pozicija++) {
                View elementListeIgraca = adapterZaIgrace.getView(pozicija, null, viewGroup);
                elementListeIgraca.measure(0, 0);
                visinaListeIgraca += elementListeIgraca.getMeasuredHeight();
            }
            ViewGroup.LayoutParams parametriLayouta = lvRangLista.getLayoutParams();
            parametriLayouta.height = visinaListeIgraca + ((brojElemenata - 1) * lvRangLista.getDividerHeight());
            lvRangLista.setLayoutParams(parametriLayouta);
            lvRangLista.requestLayout();
        }
    }
}
