package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ba.unsa.etf.rma.R;

public class InformacijeFrag extends Fragment {

    private OnButtonClick onButtonClick;
    private String nazivTrenutnogKviza;
    private int brojTacnihPitanja;
    private int brojPreostalihPitanja;
    private double procenatTacnihOdgovora;
    private TextView infNazivKviza;
    private TextView infBrojTacnihPitanja;
    private TextView infBrojPreostalihPitanja;
    private TextView infProcenatTacni;
    private Button btnKraj;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.informacije_frag, container, false);
        if (getArguments().containsKey("bundleNazivKvizaZaIgranje")) {
            nazivTrenutnogKviza = getArguments().getString("bundleNazivKvizaZaIgranje");
            brojTacnihPitanja = getArguments().getInt("bundleBrojTacnihPitanja");
            brojPreostalihPitanja = getArguments().getInt("bundleBrojPreostalihPitanja");
            procenatTacnihOdgovora = getArguments().getDouble("bundleProcenatTacnihOdgovora");
            dobaviWidgete(view);
            infNazivKviza.setText(nazivTrenutnogKviza);
            infBrojTacnihPitanja.setText(String.valueOf(brojTacnihPitanja));
            infBrojPreostalihPitanja.setText(String.valueOf(brojPreostalihPitanja));
            double zaokruzeniProcenatTacnihOdgovora = Math.round(procenatTacnihOdgovora * 100.0) / 100.0;
            String procenatZaPrikazati = String.valueOf(zaokruzeniProcenatTacnihOdgovora) + "%";
            infProcenatTacni.setText(procenatZaPrikazati);
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            onButtonClick = (OnButtonClick) getActivity();
        } catch (ClassCastException greska) {
            throw new ClassCastException("Problem sa interfejsom - treba ga implementirati!");
        }
        postaviListenerNaDugmeKrajKviza();
    }

    private void dobaviWidgete(View view) {
        infNazivKviza = (TextView) view.findViewById(R.id.infNazivKviza);
        infBrojTacnihPitanja = (TextView) view.findViewById(R.id.infBrojTacnihPitanja);
        infBrojPreostalihPitanja = (TextView) view.findViewById(R.id.infBrojPreostalihPitanja);
        infProcenatTacni = (TextView) view.findViewById(R.id.infProcenatTacni);
        btnKraj = (Button) view.findViewById(R.id.btnKraj);
    }

    private void postaviListenerNaDugmeKrajKviza() {
        btnKraj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClick.onButtonClicked();
            }
        });
    }

    public interface OnButtonClick {
        void onButtonClicked();
    }
}
