package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class Kviz implements Parcelable, Serializable {

    // Implementuje ove interfejse zbog prebacivanja izmedju aktivnosti putem intenta

    private String naziv;
    private ArrayList<Pitanje> pitanja;
    private Kategorija kategorija;
    private String idIzFirebasea;

    public Kviz() {
        naziv = "";
        pitanja = new ArrayList<>();
    }

    public Kviz(String naziv, ArrayList<Pitanje> pitanja, Kategorija kategorija) {
        this.naziv = naziv;
        this.pitanja = pitanja;
        this.kategorija = kategorija;
    }

    public Kviz(String naziv, ArrayList<Pitanje> pitanja, Kategorija kategorija, String idIzFirebasea) {
        this.naziv = naziv;
        this.pitanja = pitanja;
        this.kategorija = kategorija;
        this.idIzFirebasea = idIzFirebasea;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Pitanje> getPitanja() {
        return pitanja;
    }

    public void setPitanja(ArrayList<Pitanje> pitanja) {
        this.pitanja = pitanja;
    }

    public Kategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(Kategorija kategorija) {
        this.kategorija = kategorija;
    }

    public String getIdIzFirebasea() {
        return idIzFirebasea;
    }

    public void setIdIzFirebasea(String idIzFirebasea) {
        this.idIzFirebasea = idIzFirebasea;
    }

    public void dodajPitanje(Pitanje pitanje) {
        pitanja.add(pitanje);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeTypedList(pitanja);
        dest.writeParcelable(kategorija, flags);
        dest.writeString(idIzFirebasea);
    }

    public static final Creator<Kviz> CREATOR = new Creator<Kviz>() {
        @Override
        public Kviz createFromParcel(Parcel in) {
            return new Kviz(in);
        }

        @Override
        public Kviz[] newArray(int size) {
            return new Kviz[size];
        }
    };

    protected Kviz(Parcel in) {
        naziv = in.readString();
        pitanja = in.createTypedArrayList(Pitanje.CREATOR);
        kategorija = in.readParcelable(Kategorija.class.getClassLoader());
        idIzFirebasea = in.readString();
    }

    // Ove dodatne metode su zbog interfejsa Parcelable
}
