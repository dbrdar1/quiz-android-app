package ba.unsa.etf.rma.resultreceiveri;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class DodavanjeElementaURanglistuReceiver extends ResultReceiver {

    private Receiver receiver;

    public DodavanjeElementaURanglistuReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public interface Receiver {
        void naSaznanjeOIzmijenjenojRangListi(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (receiver != null) receiver.naSaznanjeOIzmijenjenojRangListi(resultCode, resultData);
    }
}
