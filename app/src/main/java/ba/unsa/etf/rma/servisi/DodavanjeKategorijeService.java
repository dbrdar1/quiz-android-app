package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;

public class DodavanjeKategorijeService extends IntentService {

    public static final int KATEGORIJA_DODANA = 0;
    private Kategorija kategorijaZaDodavanje = new Kategorija();

    public DodavanjeKategorijeService() {
        super(null);
    }

    public DodavanjeKategorijeService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("stat", "dodavanjeKategorijePokrenuto");
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraDodavanjeKategorijeReceiver");
        kategorijaZaDodavanje = (Kategorija) intent.getSerializableExtra("extraKategorijaZaDodavanje");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);
            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Kategorije?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            JSONObject kategorijaObject = new JSONObject();
            JSONObject poljaObject = new JSONObject();
            JSONObject nazivObject = new JSONObject();
            JSONObject idIkoniceObject = new JSONObject();
            try {
                nazivObject.put("stringValue", kategorijaZaDodavanje.getNaziv());
                idIkoniceObject.put("integerValue", kategorijaZaDodavanje.getId());
                poljaObject.put("naziv", nazivObject);
                poljaObject.put("idIkonice", idIkoniceObject);
                kategorijaObject.put("fields", poljaObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String dokument = kategorijaObject.toString();
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = dokument.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                String dobijenoNazad = response.toString();
                Log.d("ODGOVOR", response.toString());
                JSONObject dobijeniNazadObject = new JSONObject(dobijenoNazad);
                String spojeniNizString = dobijeniNazadObject.getString("name");
                String[] nizString = spojeniNizString.split("/");
                int duzinaNizStringa = nizString.length;
                String idDokumentaKategorijeString = nizString[duzinaNizStringa - 1];
                bundle.putString("extraVraceniIdKategorije", idDokumentaKategorijeString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            resultReceiver.send(KATEGORIJA_DODANA, bundle);
            Log.d("status", "zavrsilo dodavanje kategorije");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
