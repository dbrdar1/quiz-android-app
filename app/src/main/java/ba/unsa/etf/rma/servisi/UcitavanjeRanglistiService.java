package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.ElementRangListe;
import ba.unsa.etf.rma.klase.RangListaObjekat;

public class UcitavanjeRanglistiService extends IntentService {

    public static final int RANGLISTE_UCITANE = 0;
    private ArrayList<RangListaObjekat> ucitaneRangListe = new ArrayList<>();

    public UcitavanjeRanglistiService() {
        super(null);
    }

    public UcitavanjeRanglistiService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("stat", "ucitaRangPokrenuto");
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraUcitavanjeRangListiReceiver");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Rangliste?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                Log.d("ODGOVORRangliste", rezultujuciJsonString);
                JSONObject jsonObject = new JSONObject(rezultujuciJsonString);
                JSONArray dokumentiArray = new JSONArray();
                try {
                    dokumentiArray = jsonObject.getJSONArray("documents");
                } catch (JSONException e) {
                    bundle.putParcelableArrayList("extraUcitaneRangListe", ucitaneRangListe);
                    resultReceiver.send(RANGLISTE_UCITANE, bundle);
                    Log.d("status", "zavrsilo ucitavanje ranglisti");
                    return;
                }
                for (int i = 0; i < dokumentiArray.length(); i++) {
                    RangListaObjekat ucitanaRangLista = new RangListaObjekat();
                    JSONObject pojedinacniDokumentObject = dokumentiArray.getJSONObject(i);
                    String spojeniNizString = pojedinacniDokumentObject.getString("name");
                    String[] nizString = spojeniNizString.split("/");
                    int duzinaNizStringa = nizString.length;
                    String idDokumentaRangListeString = nizString[duzinaNizStringa - 1];
                    JSONObject poljaObject = pojedinacniDokumentObject.getJSONObject("fields");
                    JSONObject nazivKvizaObject = poljaObject.getJSONObject("nazivKviza");
                    String nazivKvizaString = nazivKvizaObject.getString("stringValue");
                    JSONObject listaObject = poljaObject.getJSONObject("lista");
                    JSONObject listaMapValue = listaObject.getJSONObject("mapValue");
                    JSONObject poljaMapeObject = listaMapValue.getJSONObject("fields");
                    int brojac = 1;
                    while (true) {
                        try {
                            JSONObject redniBrojObject = poljaMapeObject.getJSONObject(String.valueOf(brojac));
                            JSONObject unutarnjaMapaObject = redniBrojObject.getJSONObject("mapValue");
                            JSONObject unutarnjaPoljaMapeObject = unutarnjaMapaObject.getJSONObject("fields");
                            String fieldsUString  = unutarnjaPoljaMapeObject.toString();
                            String[] nizPoNavodnicima = fieldsUString.split("\"");
                            String kljucImeIgraca = nizPoNavodnicima[1];
                            JSONObject procenatObject = unutarnjaPoljaMapeObject.getJSONObject(kljucImeIgraca);
                            String procenat = procenatObject.getString("stringValue");
                            ucitanaRangLista.dodajNoviElementUListu(new ElementRangListe(kljucImeIgraca, procenat));
                        } catch (Exception e) {
                            break;
                        }
                        brojac++;
                    }
                    ucitanaRangLista.setIdIzFirebasea(idDokumentaRangListeString);
                    ucitanaRangLista.setNazivKviza(nazivKvizaString);
                    ucitanaRangLista.sortirajListu();
                    ucitaneRangListe.add(ucitanaRangLista);
                }
                bundle.putSerializable("extraUcitaneRangListe", ucitaneRangListe);
                resultReceiver.send(RANGLISTE_UCITANE, bundle);
                Log.d("status", "zavrsile rangliste");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
