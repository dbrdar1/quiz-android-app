package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class ElementRangListe implements Parcelable, Serializable, Comparable<ElementRangListe> {

    // Implementuje ove interfejse zbog prebacivanja

    private String imeIgraca;
    private String procenatTacnosti;

    public ElementRangListe() {
    }

    public ElementRangListe(String imeIgraca, String procenatTacnosti) {
        this.imeIgraca = imeIgraca;
        this.procenatTacnosti = procenatTacnosti;
    }

    public String getImeIgraca() {
        return imeIgraca;
    }

    public void setImeIgraca(String imeIgraca) {
        this.imeIgraca = imeIgraca;
    }

    public String getProcenatTacnosti() {
        return procenatTacnosti;
    }

    public void setProcenatTacnosti(String procenatTacnosti) {
        this.procenatTacnosti = procenatTacnosti;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imeIgraca);
        dest.writeString(procenatTacnosti);
    }

    public static final Parcelable.Creator<ElementRangListe> CREATOR = new Parcelable.Creator<ElementRangListe>() {
        @Override
        public ElementRangListe createFromParcel(Parcel in) {
            return new ElementRangListe(in);
        }

        @Override
        public ElementRangListe[] newArray(int size) {
            return new ElementRangListe[size];
        }
    };

    protected ElementRangListe(Parcel in) {
        imeIgraca = in.readString();
        procenatTacnosti = in.readString();
    }

    // Ove dodatne metode su zbog interfejsa Parcelable

    @Override
    public String toString() {
        return imeIgraca + ", sa procentom " + procenatTacnosti + "%";
    }

    @Override
    public int compareTo(ElementRangListe o) {
        double procenatTrenutnog = Double.parseDouble(procenatTacnosti);
        double procenatPoredjenog = Double.parseDouble(o.getProcenatTacnosti());
        if (procenatTrenutnog != procenatPoredjenog) return Double.compare(procenatPoredjenog, procenatTrenutnog);
        return 1;
    }
}
