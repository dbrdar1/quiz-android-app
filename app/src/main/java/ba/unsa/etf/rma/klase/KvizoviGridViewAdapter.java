package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class KvizoviGridViewAdapter extends BaseAdapter {

    private Activity aktivnost;
    private ArrayList<Kviz> listaKvizova;
    private static LayoutInflater layoutInflater = null;
    private Resources resursi;

    public KvizoviGridViewAdapter(Activity aktivnost, ArrayList<Kviz> listaKvizova, Resources resursi) {
        this.aktivnost = aktivnost;
        this.listaKvizova = listaKvizova;
        this.resursi = resursi;
        layoutInflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaKvizova.size();
    }

    @Override
    public Kviz getItem(int position) {
        return listaKvizova.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) convertView = layoutInflater.inflate(R.layout.element_grid_viewa_kvizova, null);
        IconView iconView = (IconView) convertView.findViewById(R.id.icIkonaKategorije);
        iconView.setIcon(Integer.parseInt(listaKvizova.get(position).getKategorija().getId()));
        TextView tvNazivKviza = (TextView) convertView.findViewById(R.id.tvNazivKviza);
        tvNazivKviza.setText(listaKvizova.get(position).getNaziv());
        TextView tvBrojPitanjaKviza = (TextView) convertView.findViewById(R.id.tvBrojPitanjaKviza);
        if (position != listaKvizova.size() - 1)
            tvBrojPitanjaKviza.setText(String.valueOf(listaKvizova.get(position).getPitanja().size()));
        else
            tvBrojPitanjaKviza.setText("");
        postaviPozadinuElementa(convertView, resursi.getColor(R.color.colorLighterBlue));
        return convertView;
    }

    private void postaviPozadinuElementa(View view, int boja) {
        view.setBackgroundColor(boja);
    }
}
