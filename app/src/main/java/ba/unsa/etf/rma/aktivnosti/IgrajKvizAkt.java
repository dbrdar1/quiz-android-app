package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.AlarmClock;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.broadcastreceiveri.InternetBroadcastReceiver;
import ba.unsa.etf.rma.fragmenti.InformacijeFrag;
import ba.unsa.etf.rma.fragmenti.PitanjeFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.ElementRangListe;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.RangListaObjekat;
import ba.unsa.etf.rma.korisneklase.MjestoZaCuvanjeNovihRangListi;
import ba.unsa.etf.rma.korisneklase.OsvjezivacPodatakaUBazama;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeRanglistePoNazivuKvizaReceiver;
import ba.unsa.etf.rma.servisi.DodavanjeElementaURanglistuService;
import ba.unsa.etf.rma.servisi.DodavanjeRanglisteService;
import ba.unsa.etf.rma.servisi.UcitavanjeRanglistePoNazivuKvizaService;
import ba.unsa.etf.rma.sqliteopenhelperi.BazaSQLiteOpenHelper;

public class IgrajKvizAkt extends AppCompatActivity implements PitanjeFrag.OnItemClick, PitanjeFrag.ZahtijevanjeImenaIgraca,
        InformacijeFrag.OnButtonClick, UcitavanjeRanglistePoNazivuKvizaReceiver.Receiver,
        InternetBroadcastReceiver.Aktivnost {

    private int redniBrojTrenutnogPitanja;
    private int brojTacnihPitanja;
    private int brojPreostalihPitanja;
    private double procenatTacnihOdgovora;
    private ArrayList<Pitanje> listaRandomPitanja = new ArrayList<>();
    private Kviz trenutniKviz;
    private FragmentManager fragmentManager;
    private InformacijeFrag informacijeFrag;
    private PitanjeFrag pitanjeFrag;
    private RangLista rangLista;
    private RangListaObjekat rangListaKviza = new RangListaObjekat();
    private String imeIgraca = "";
    private InternetBroadcastReceiver internetBroadcastReceiver = new InternetBroadcastReceiver();
    private IntentFilter internetIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    private boolean imaInternetKonekcije;
    private BazaSQLiteOpenHelper bazaSQLiteOpenHelper;
    private boolean prvo;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.igraj_kviz_akt);
        progressDialog = new ProgressDialog(IgrajKvizAkt.this);
        pokupiPoslanePodatke();
        postaviAlarm();
        promijesajPitanjaURandomPoretku();
        inicijalizirajFragmentManager();
        postaviFragmentInformacije();
        postaviFragmentPitanje();
        bazaSQLiteOpenHelper = new BazaSQLiteOpenHelper(this, BazaSQLiteOpenHelper.NAZIV_BAZE,
                null, BazaSQLiteOpenHelper.VERZIJA_BAZE);
    }

    private void pokupiPoslanePodatke() {
        trenutniKviz = (Kviz) getIntent().getSerializableExtra("extraKvizZaIgranje");
        listaRandomPitanja.addAll(trenutniKviz.getPitanja());
    }

    private void postaviAlarm() {
        int brojPitanjaKviza = trenutniKviz.getPitanja().size();
        if (brojPitanjaKviza == 0) return;
        int brojMinutaZaDodavanje = brojPitanjaKviza / 2;
        if (jeLiNeparanBroj(brojPitanjaKviza)) brojMinutaZaDodavanje += 1;
        Calendar kalendar = new GregorianCalendar();
        kalendar.setTimeInMillis(System.currentTimeMillis());
        int trenutneSekunde = kalendar.get(Calendar.SECOND);
        if (trenutneSekunde != 0) {
            kalendar.add(Calendar.MINUTE, 1);
            kalendar.set(Calendar.SECOND, 0);
        }
        kalendar.add(Calendar.MINUTE, brojMinutaZaDodavanje);
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
        intent.putExtra(AlarmClock.EXTRA_HOUR, kalendar.get(Calendar.HOUR_OF_DAY));
        intent.putExtra(AlarmClock.EXTRA_MINUTES, kalendar.get(Calendar.MINUTE));
        intent.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
        startActivity(intent);
    }

    private boolean jeLiNeparanBroj(int brojZaTestiranje) {
        return brojZaTestiranje % 2 != 0;
    }

    private void promijesajPitanjaURandomPoretku() {
        Collections.shuffle(listaRandomPitanja);
    }

    private void inicijalizirajFragmentManager() {
        fragmentManager = getSupportFragmentManager();
    }

    private void postaviFragmentInformacije() {
        FrameLayout informacijePlace = (FrameLayout) findViewById(R.id.informacijePlace);
        if (informacijePlace != null) {
            informacijeFrag = (InformacijeFrag) fragmentManager.findFragmentById(R.id.informacijePlace);
            if (informacijeFrag == null) {
                informacijeFrag = new InformacijeFrag();
                Bundle argumentiFragmentaInformacije = new Bundle();
                brojTacnihPitanja = 0;
                brojPreostalihPitanja = listaRandomPitanja.size();
                procenatTacnihOdgovora = 0;
                argumentiFragmentaInformacije.putString("bundleNazivKvizaZaIgranje", trenutniKviz.getNaziv());
                argumentiFragmentaInformacije.putInt("bundleBrojTacnihPitanja", brojTacnihPitanja);
                if (brojPreostalihPitanja == 0) {
                    argumentiFragmentaInformacije.putInt("bundleBrojPreostalihPitanja", brojPreostalihPitanja);
                }
                else {
                    argumentiFragmentaInformacije.putInt("bundleBrojPreostalihPitanja", brojPreostalihPitanja - 1);
                }
                argumentiFragmentaInformacije.putDouble("bundleProcenatTacnihOdgovora", procenatTacnihOdgovora * 100);
                informacijeFrag.setArguments(argumentiFragmentaInformacije);
                fragmentManager.beginTransaction().replace(R.id.informacijePlace, informacijeFrag).commit();
            }
        }
    }

    private void postaviFragmentPitanje() {
        FrameLayout pitanjePlace = (FrameLayout) findViewById(R.id.pitanjePlace);
        if (pitanjePlace != null) {
            pitanjeFrag = (PitanjeFrag) fragmentManager.findFragmentById(R.id.pitanjePlace);
            if (pitanjeFrag == null) {
                pitanjeFrag = new PitanjeFrag();
                Bundle argumentiFragmentaPitanje = new Bundle();
                if (brojPreostalihPitanja != 0) {
                    argumentiFragmentaPitanje.putSerializable("bundlePitanjeZaOdgovaranje",
                            listaRandomPitanja.get(redniBrojTrenutnogPitanja));
                }
                pitanjeFrag.setArguments(argumentiFragmentaPitanje);
                fragmentManager.beginTransaction().replace(R.id.pitanjePlace, pitanjeFrag).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intentPovratakNaKvizove = new Intent();
        setResult(RESULT_CANCELED, intentPovratakNaKvizove);
        finish();
    }

    @Override
    public void onItemClicked(int position, boolean tacanOdgovorKliknut) {
        informacijeFrag = new InformacijeFrag();
        Bundle argumentiFragmentaInformacije = new Bundle();
        argumentiFragmentaInformacije.putString("bundleNazivKvizaZaIgranje", trenutniKviz.getNaziv());
        if (tacanOdgovorKliknut) brojTacnihPitanja++;
        argumentiFragmentaInformacije.putInt("bundleBrojTacnihPitanja", brojTacnihPitanja);
        brojPreostalihPitanja--;

        // Broj preostalih pitanja je broj pitanja koja jos uvijek nisu prikazana

        if (brojPreostalihPitanja == 0) {
            argumentiFragmentaInformacije.putInt("bundleBrojPreostalihPitanja", brojPreostalihPitanja);
        }
        else {
            argumentiFragmentaInformacije.putInt("bundleBrojPreostalihPitanja", brojPreostalihPitanja - 1);
        }
        if ((listaRandomPitanja.size() != 0) && (brojPreostalihPitanja != listaRandomPitanja.size())) {
            procenatTacnihOdgovora = ((double) brojTacnihPitanja) / (listaRandomPitanja.size() - brojPreostalihPitanja);
        }
        else procenatTacnihOdgovora = 0;
        argumentiFragmentaInformacije.putDouble("bundleProcenatTacnihOdgovora", procenatTacnihOdgovora * 100);
        informacijeFrag.setArguments(argumentiFragmentaInformacije);
        getSupportFragmentManager().beginTransaction().replace(R.id.informacijePlace, informacijeFrag).commit();
        pitanjeFrag = new PitanjeFrag();
        Bundle argumentiFragmentaPitanje = new Bundle();
        if (brojPreostalihPitanja != 0) {
            argumentiFragmentaPitanje.putSerializable("bundlePitanjeZaOdgovaranje",
                    listaRandomPitanja.get(++redniBrojTrenutnogPitanja));
        }
        pitanjeFrag.setArguments(argumentiFragmentaPitanje);
        fragmentManager.beginTransaction().replace(R.id.pitanjePlace, pitanjeFrag).commit();
    }

    @Override
    public void onButtonClicked() {
        Intent intentPovratakNaKvizove = new Intent();
        setResult(RESULT_OK, intentPovratakNaKvizove);
        finish();
    }

    @Override
    public void pitajIgracaZaIme() {
        prikaziAlertZahtijevanjeImenaIgraca();
    }

    private void prikaziAlertZahtijevanjeImenaIgraca() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Unesite svoje ime:");
        final EditText editText = new EditText(this);
        alertDialogBuilder.setView(editText);
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        final AlertDialog dijalogZaIme = alertDialogBuilder.create();
        dijalogZaIme.show();
        dijalogZaIme.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imeIgraca = editText.getText().toString();
                if (imeIgraca.trim().length() != 0) {
                    dijalogZaIme.dismiss();
                    if (imaInternetKonekcije) ucitajRangListuIzFirebasea();
                    else ucitajIAzurirajRangListuIzSQLitea();
                }
            }
        });
    }

    private void ucitajRangListuIzFirebasea() {
        UcitavanjeRanglistePoNazivuKvizaReceiver ucitavanjeRanglistePoNazivuKvizaReceiver = new UcitavanjeRanglistePoNazivuKvizaReceiver(new Handler());
        ucitavanjeRanglistePoNazivuKvizaReceiver.setReceiver(this);
        Intent intent = new Intent(this, UcitavanjeRanglistePoNazivuKvizaService.class);
        intent.putExtra("extraUcitavanjeRangListePoNazivuKvizaReceiver", ucitavanjeRanglistePoNazivuKvizaReceiver);
        intent.putExtra("extraNazivZadanogKviza", trenutniKviz.getNaziv());
        startService(intent);
    }

    private void ucitajIAzurirajRangListuIzSQLitea() {
        String[] projekcija = new String[] { BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_IME_IGRACA,
        BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_PROCENAT };
        String selekcija = "(" + BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_KVIZ + " = ?)";
        String[] argumentiSelekcije = new String[] { trenutniKviz.getNaziv() };
        SQLiteDatabase db = bazaSQLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(BazaSQLiteOpenHelper.NAZIV_TABELE_ELEMENTI_RANGLISTE, projekcija, selekcija,
                argumentiSelekcije, null, null, null);
        RangListaObjekat primljenaLista = new RangListaObjekat();
        primljenaLista.setNazivKviza(trenutniKviz.getNaziv());
        if (cursor != null) {
            if (cursor.getCount() != 0) { // vec ima ranglista s tim nazivom kviza
                while (cursor.moveToNext()) {
                    String primljenoImeIgraca = cursor.getString(cursor.getColumnIndex
                            (BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_IME_IGRACA));
                    String primljeniProcenatTacnosti = cursor.getString(cursor.getColumnIndex
                            (BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_PROCENAT));
                    primljenaLista.dodajNoviElementUListu(new ElementRangListe(primljenoImeIgraca, primljeniProcenatTacnosti));
                }
                rangListaKviza = primljenaLista;
                double tempProcenat = procenatTacnihOdgovora * 100;
                double zaokruzeniProcenatTacnihOdgovora = Math.round(tempProcenat * 100.0) / 100.0;
                String procenatPretvorenUString = String.valueOf(zaokruzeniProcenatTacnihOdgovora);
                ElementRangListe noviElementRangListe = new ElementRangListe(imeIgraca, procenatPretvorenUString);
                rangListaKviza.dodajNoviElementUListu(noviElementRangListe);
                rangListaKviza.sortirajListu();
                if (MjestoZaCuvanjeNovihRangListi.daLiPostojiRangListaSaNazivomKviza(trenutniKviz.getNaziv())) {
                    MjestoZaCuvanjeNovihRangListi.dodajNoviElementURangListu(trenutniKviz.getNaziv(), noviElementRangListe);
                }
                else {
                    RangListaObjekat novaRangLista = new RangListaObjekat();
                    novaRangLista.setNazivKviza(trenutniKviz.getNaziv());
                    novaRangLista.dodajNoviElementUListu(noviElementRangListe);
                    novaRangLista.sortirajListu();
                    MjestoZaCuvanjeNovihRangListi.dodajNovuRangListu(novaRangLista);
                }
                izmijeniPostojecuRangListuUSQLiteu(noviElementRangListe);
                Toast.makeText(this, "Izmijenjena ranglista lokalno", Toast.LENGTH_SHORT).show();
                postaviFragmentRangLista();
            }
            else { // nema rangliste s tim nazivom kviza
                rangListaKviza.setNazivKviza(trenutniKviz.getNaziv());
                double tempProcenat = procenatTacnihOdgovora * 100;
                double zaokruzeniProcenatTacnihOdgovora = Math.round(tempProcenat * 100.0) / 100.0;
                String procenatPretvorenUString = String.valueOf(zaokruzeniProcenatTacnihOdgovora);
                ElementRangListe noviElementRangListe = new ElementRangListe(imeIgraca, procenatPretvorenUString);
                rangListaKviza.dodajNoviElementUListu(noviElementRangListe);
                rangListaKviza.sortirajListu();
                if (MjestoZaCuvanjeNovihRangListi.daLiPostojiRangListaSaNazivomKviza(trenutniKviz.getNaziv())) {
                    MjestoZaCuvanjeNovihRangListi.dodajNoviElementURangListu(trenutniKviz.getNaziv(), noviElementRangListe);
                }
                else {
                    RangListaObjekat novaRangLista = new RangListaObjekat();
                    novaRangLista.setNazivKviza(trenutniKviz.getNaziv());
                    novaRangLista.dodajNoviElementUListu(noviElementRangListe);
                    novaRangLista.sortirajListu();
                    MjestoZaCuvanjeNovihRangListi.dodajNovuRangListu(novaRangLista);
                }
                dodajNovuRangListuUSQLite();
                Toast.makeText(this, "Dodana ranglista lokalno", Toast.LENGTH_SHORT).show();
                postaviFragmentRangLista();
            }
        }
        if (cursor != null) cursor.close();
        db.close();
    }

    private void izmijeniPostojecuRangListuUSQLiteu(ElementRangListe elementRangListe) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_KVIZ, rangListaKviza.getNazivKviza());
        contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_IME_IGRACA, elementRangListe.getImeIgraca());
        contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_PROCENAT, elementRangListe.getProcenatTacnosti());
        SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
        db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_ELEMENTI_RANGLISTE, null, contentValues);
        db.close();
    }

    private void dodajNovuRangListuUSQLite() {
        upisiElementeRangListeUSQLite();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BazaSQLiteOpenHelper.RANGLISTA_NAZIV_KVIZA, rangListaKviza.getNazivKviza());
        SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
        db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_RANGLISTE, null, contentValues);
        db.close();
    }

    private void upisiElementeRangListeUSQLite() {
        ArrayList<ElementRangListe> elementiRangListe = rangListaKviza.getLista();
        for (ElementRangListe elementRangListe : elementiRangListe) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_KVIZ, rangListaKviza.getNazivKviza());
            contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_IME_IGRACA, elementRangListe.getImeIgraca());
            contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_PROCENAT, elementRangListe.getProcenatTacnosti());
            SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
            db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_ELEMENTI_RANGLISTE, null, contentValues);
            db.close();
        }
    }

    private void postaviFragmentRangLista() {
        FrameLayout pitanjePlace = (FrameLayout) findViewById(R.id.pitanjePlace);
        if (pitanjePlace != null) {
            rangLista = new RangLista();
            Bundle argumentiFragmentaRangLista = new Bundle();
            ArrayList<String> rangListaIgraca = new ArrayList<>();
            for (int i = 0; i < rangListaKviza.getLista().size(); i++) {
                rangListaIgraca.add(String.valueOf(i + 1) + ". " + rangListaKviza.getLista().get(i).toString());
            }
            argumentiFragmentaRangLista.putStringArrayList("bundleRangListaIgraca", rangListaIgraca);
            rangLista.setArguments(argumentiFragmentaRangLista);
            fragmentManager.beginTransaction().replace(R.id.pitanjePlace, rangLista).commit();
        }
    }

    @Override
    public void naPrimanjeUcitaneRangListePoNazivuKviza(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeRanglistePoNazivuKvizaService.NEMA_RANGLISTE_U_BAZI) {
            rangListaKviza.setNazivKviza(trenutniKviz.getNaziv());
            double tempProcenat = procenatTacnihOdgovora * 100;
            double zaokruzeniProcenatTacnihOdgovora = Math.round(tempProcenat * 100.0) / 100.0;
            String procenatPretvorenUString = String.valueOf(zaokruzeniProcenatTacnihOdgovora);
            rangListaKviza.dodajNoviElementUListu(new ElementRangListe(imeIgraca, procenatPretvorenUString));
            rangListaKviza.sortirajListu();
            dodajNovuRangListuUFirebase();
            postaviFragmentRangLista();
        }
        else if (resultCode == UcitavanjeRanglistePoNazivuKvizaService.RANGLISTA_UCITANA_PO_NAZIVU_KVIZA) {
            RangListaObjekat primljenaLista = (RangListaObjekat) resultData.getSerializable("extraUcitanaRangListaPoKvizu");
            if (primljenaLista != null) rangListaKviza = primljenaLista;
            double tempProcenat = procenatTacnihOdgovora * 100;
            double zaokruzeniProcenatTacnihOdgovora = Math.round(tempProcenat * 100.0) / 100.0;
            String procenatPretvorenUString = String.valueOf(zaokruzeniProcenatTacnihOdgovora);
            rangListaKviza.dodajNoviElementUListu(new ElementRangListe(imeIgraca, procenatPretvorenUString));
            rangListaKviza.sortirajListu();
            izmijeniPostojecuRangListuUFirebaseu();
            postaviFragmentRangLista();
        }
    }

    private void dodajNovuRangListuUFirebase() {
        Intent intent = new Intent(this, DodavanjeRanglisteService.class);
        intent.putExtra("extraRangListaZaDodavanje", (Serializable) rangListaKviza);
        startService(intent);
    }

    private void izmijeniPostojecuRangListuUFirebaseu() {
        Intent intent = new Intent(this, DodavanjeElementaURanglistuService.class);
        intent.putExtra("extraRangListaZaMijenjanje", (Serializable) rangListaKviza);
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prvo = true;
        internetBroadcastReceiver.setAktivnost(this);
        registerReceiver(internetBroadcastReceiver, internetIntentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(internetBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void izmijeniStatusKonekcije(boolean imaKonekcije) {
        imaInternetKonekcije = imaKonekcije;
        if (prvo) {
            prvo = false;
            return;
        }
        if (imaKonekcije) new OsvjezivacPodatakaUBazama(this).azurirajStanjeUSQLiteuIFirebaseu();
    }
}
