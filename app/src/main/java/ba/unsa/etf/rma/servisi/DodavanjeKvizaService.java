package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodavanjeKvizaService extends IntentService {

    public static final int KVIZ_DODAN = 0;
    private Kviz kvizZaDodavanje = new Kviz();

    public DodavanjeKvizaService() {
        super(null);
    }

    public DodavanjeKvizaService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("stat", "dodavanjeKvizaPokrenuto");
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraDodavanjeKvizaReceiver");
        kvizZaDodavanje = (Kviz) intent.getSerializableExtra("extraKvizZaDodavanje");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Kvizovi?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            JSONObject kvizObject = new JSONObject();
            JSONObject poljaObject = new JSONObject();
            JSONObject nazivObject = new JSONObject();
            JSONObject idKategorijeObject = new JSONObject();
            JSONObject pitanjaObject = new JSONObject();
            JSONObject arrayValueObject = new JSONObject();
            JSONArray vrijednostiArray = new JSONArray();
            ArrayList<Pitanje> pitanjaKviza = kvizZaDodavanje.getPitanja();
            try {
                for (int i = 0; i < pitanjaKviza.size(); i++) {
                    JSONObject pojedinacniIdPitanjaObject = new JSONObject();
                    pojedinacniIdPitanjaObject.put("stringValue", pitanjaKviza.get(i).getIdIzFirebasea());
                    vrijednostiArray.put(i, pojedinacniIdPitanjaObject);
                }
                arrayValueObject.put("values", vrijednostiArray);
                nazivObject.put("stringValue", kvizZaDodavanje.getNaziv());
                idKategorijeObject.put("stringValue", kvizZaDodavanje.getKategorija().getIdIzFirebasea());
                pitanjaObject.put("arrayValue", arrayValueObject);
                poljaObject.put("naziv", nazivObject);
                poljaObject.put("idKategorije", idKategorijeObject);
                poljaObject.put("pitanja", pitanjaObject);
                kvizObject.put("fields", poljaObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String dokument = kvizObject.toString();
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = dokument.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                Log.d("ODGOVOR", response.toString());
            }
            Log.d("status", "zavrsilo dodavanje kviza");
            resultReceiver.send(KVIZ_DODAN, bundle);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
