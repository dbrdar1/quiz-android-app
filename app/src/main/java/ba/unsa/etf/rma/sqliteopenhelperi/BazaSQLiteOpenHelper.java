package ba.unsa.etf.rma.sqliteopenhelperi;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BazaSQLiteOpenHelper extends SQLiteOpenHelper {

    public static final String NAZIV_BAZE = "baza.db";
    public static final int VERZIJA_BAZE = 1;

    // Sve vezano za tabelu kvizova

    public static final String NAZIV_TABELE_KVIZOVI = "kvizovi";
    public static final String KVIZ_ID = "_id";
    public static final String KVIZ_NAZIV = "naziv";
    public static final String KVIZ_KATEGORIJA = "kategorija";
    private static final String SKRIPTA_KREIRANJE_TABELE_KVIZOVI =
            "create table " + NAZIV_TABELE_KVIZOVI + " (" +
                    KVIZ_ID + " integer primary key autoincrement, " +
                    KVIZ_NAZIV + " text, " +
                    KVIZ_KATEGORIJA + " text);";
    private static final String SKRIPTA_BRISANJE_TABELE_KVIZOVI =
            "drop table if exists " + NAZIV_TABELE_KVIZOVI;

    // Sve vezano za tabelu pitanja

    public static final String NAZIV_TABELE_PITANJA = "pitanja";
    public static final String PITANJE_ID = "_id";
    public static final String PITANJE_NAZIV = "naziv";
    public static final String PITANJE_TACAN = "tacan";
    private static final String SKRIPTA_KREIRANJE_TABELE_PITANJA =
            "create table " + NAZIV_TABELE_PITANJA + " (" +
                    PITANJE_ID + " integer primary key autoincrement, " +
                    PITANJE_NAZIV + " text, " +
                    PITANJE_TACAN + " text);";
    private static final String SKRIPTA_BRISANJE_TABELE_PITANJA =
            "drop table if exists " + NAZIV_TABELE_PITANJA;

    // Sve vezano za tabelu odgovora

    public static final String NAZIV_TABELE_ODGOVORI = "odgovori";
    public static final String ODGOVOR_ID = "_id";
    public static final String ODGOVOR_PITANJE = "pitanje";
    public static final String ODGOVOR_TEKST = "tekst";
    private static final String SKRIPTA_KREIRANJE_TABELE_ODGOVORI =
            "create table " + NAZIV_TABELE_ODGOVORI + " (" +
                    ODGOVOR_ID + " integer primary key autoincrement, " +
                    ODGOVOR_PITANJE + " text, " +
                    ODGOVOR_TEKST + " text);";
    private static final String SKRIPTA_BRISANJE_TABELE_ODGOVORI =
            "drop table if exists " + NAZIV_TABELE_ODGOVORI;

    // Sve vezano za poveznu vise-na-vise tabelu pitanja i kvizova

    public static final String NAZIV_TABELE_PITANJE_U_KVIZU = "pitanje_u_kvizu";
    public static final String PITANJE_U_KVIZU_ID = "_id";
    public static final String PITANJE_U_KVIZU_KVIZ = "kviz";
    public static final String PITANJE_U_KVIZU_PITANJE = "pitanje";
    private static final String SKRIPTA_KREIRANJE_TABELE_PITANJE_U_KVIZU =
            "create table " + NAZIV_TABELE_PITANJE_U_KVIZU + " (" +
                    PITANJE_U_KVIZU_ID + " integer primary key autoincrement, " +
                    PITANJE_U_KVIZU_KVIZ + " text, " +
                    PITANJE_U_KVIZU_PITANJE + " text);";
    private static final String SKRIPTA_BRISANJE_TABELE_PITANJE_U_KVIZU =
            "drop table if exists " + NAZIV_TABELE_PITANJE_U_KVIZU;

    // Sve vezano za tabelu kategorija

    public static final String NAZIV_TABELE_KATEGORIJE = "kategorije";
    public static final String KATEGORIJA_ID = "_id";
    public static final String KATEGORIJA_NAZIV = "naziv";
    public static final String KATEGORIJA_ID_IKONICE = "id_ikonice";
    private static final String SKRIPTA_KREIRANJE_TABELE_KATEGORIJE =
            "create table " + NAZIV_TABELE_KATEGORIJE + " (" +
                    KATEGORIJA_ID + " integer primary key autoincrement, " +
                    KATEGORIJA_NAZIV + " text, " +
                    KATEGORIJA_ID_IKONICE + " text);";
    private static final String SKRIPTA_BRISANJE_TABELE_KATEGORIJE =
            "drop table if exists " + NAZIV_TABELE_KATEGORIJE;

    // Sve vezano za tabelu ranglisti

    public static final String NAZIV_TABELE_RANGLISTE = "rangliste";
    public static final String RANGLISTA_ID = "_id";
    public static final String RANGLISTA_NAZIV_KVIZA = "naziv_kviza";
    private static final String SKRIPTA_KREIRANJE_TABELE_RANGLISTE =
            "create table " + NAZIV_TABELE_RANGLISTE + " (" +
                    RANGLISTA_ID + " integer primary key autoincrement, " +
                    RANGLISTA_NAZIV_KVIZA + " text);";
    private static final String SKRIPTA_BRISANJE_TABELE_RANGLISTE =
            "drop table if exists " + NAZIV_TABELE_RANGLISTE;

    // Sve vezano za tabelu elemenata ranglisti

    public static final String NAZIV_TABELE_ELEMENTI_RANGLISTE = "elementi_rangliste";
    public static final String ELEMENT_RANGLISTE_ID = "_id";
    public static final String ELEMENT_RANGLISTE_KVIZ = "kviz";
    public static final String ELEMENT_RANGLISTE_IME_IGRACA = "ime_igraca";
    public static final String ELEMENT_RANGLISTE_PROCENAT = "procenat";
    private static final String SKRIPTA_KREIRANJE_TABELE_ELEMENTI_RANGLISTE =
            "create table " + NAZIV_TABELE_ELEMENTI_RANGLISTE + " (" +
                    ELEMENT_RANGLISTE_ID + " integer primary key autoincrement, " +
                    ELEMENT_RANGLISTE_KVIZ + " text, " +
                    ELEMENT_RANGLISTE_IME_IGRACA + " text, " +
                    ELEMENT_RANGLISTE_PROCENAT + " text);";
    private static final String SKRIPTA_BRISANJE_TABELE_ELEMENTI_RANGLISTE =
            "drop table if exists " + NAZIV_TABELE_ELEMENTI_RANGLISTE;

    public BazaSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SKRIPTA_KREIRANJE_TABELE_KVIZOVI);
        db.execSQL(SKRIPTA_KREIRANJE_TABELE_PITANJA);
        db.execSQL(SKRIPTA_KREIRANJE_TABELE_ODGOVORI);
        db.execSQL(SKRIPTA_KREIRANJE_TABELE_PITANJE_U_KVIZU);
        db.execSQL(SKRIPTA_KREIRANJE_TABELE_RANGLISTE);
        db.execSQL(SKRIPTA_KREIRANJE_TABELE_ELEMENTI_RANGLISTE);
        db.execSQL(SKRIPTA_KREIRANJE_TABELE_KATEGORIJE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SKRIPTA_BRISANJE_TABELE_KATEGORIJE);
        db.execSQL(SKRIPTA_BRISANJE_TABELE_ELEMENTI_RANGLISTE);
        db.execSQL(SKRIPTA_BRISANJE_TABELE_RANGLISTE);
        db.execSQL(SKRIPTA_BRISANJE_TABELE_PITANJE_U_KVIZU);
        db.execSQL(SKRIPTA_BRISANJE_TABELE_ODGOVORI);
        db.execSQL(SKRIPTA_BRISANJE_TABELE_PITANJA);
        db.execSQL(SKRIPTA_BRISANJE_TABELE_KVIZOVI);
        onCreate(db);
    }
}
