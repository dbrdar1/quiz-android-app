package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Pitanje implements Parcelable, Serializable {

    // Implementuje ove interfejse zbog prebacivanja izmedju aktivnosti putem intenta

    private String naziv;
    private String tekstPitanja;
    private ArrayList<String> odgovori;
    private String tacan;
    private String idIzFirebasea;

    public Pitanje() {
        odgovori = new ArrayList<>();
    }

    public Pitanje(String naziv, String tekstPitanja, ArrayList<String> odgovori, String tacan) {
        this.naziv = naziv;
        this.tekstPitanja = tekstPitanja;
        this.odgovori = odgovori;
        this.tacan = tacan;
    }

    public Pitanje(String naziv, String tekstPitanja, ArrayList<String> odgovori, String tacan, String idIzFirebasea) {
        this.naziv = naziv;
        this.tekstPitanja = tekstPitanja;
        this.odgovori = odgovori;
        this.tacan = tacan;
        this.idIzFirebasea = idIzFirebasea;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getTekstPitanja() {
        return tekstPitanja;
    }

    public void setTekstPitanja(String tekstPitanja) {
        this.tekstPitanja = tekstPitanja;
    }

    public ArrayList<String> getOdgovori() {
        return odgovori;
    }

    public void setOdgovori(ArrayList<String> odgovori) {
        this.odgovori = odgovori;
    }

    public String getTacan() {
        return tacan;
    }

    public void setTacan(String tacan) {
        this.tacan = tacan;
    }

    public String getIdIzFirebasea() {
        return idIzFirebasea;
    }

    public void setIdIzFirebasea(String idIzFirebasea) {
        this.idIzFirebasea = idIzFirebasea;
    }

    public ArrayList<String> dajRandomOdgovore() {
        ArrayList<String> promijesanaLista = new ArrayList<>();
        promijesanaLista.addAll(getOdgovori());
        Collections.shuffle(promijesanaLista);
        return promijesanaLista;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeString(tekstPitanja);
        dest.writeStringList(odgovori);
        dest.writeString(tacan);
        dest.writeString(idIzFirebasea);
    }

    protected Pitanje(Parcel in) {
        naziv = in.readString();
        tekstPitanja = in.readString();
        odgovori = in.createStringArrayList();
        tacan = in.readString();
        idIzFirebasea = in.readString();
    }

    public static final Creator<Pitanje> CREATOR = new Creator<Pitanje>() {
        @Override
        public Pitanje createFromParcel(Parcel in) {
            return new Pitanje(in);
        }

        @Override
        public Pitanje[] newArray(int size) {
            return new Pitanje[size];
        }
    };

    // Ove dodatne metode su zbog interfejsa Parcelable
}
