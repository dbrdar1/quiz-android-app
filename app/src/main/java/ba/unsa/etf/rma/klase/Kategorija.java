package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Kategorija implements Parcelable, Serializable {

    // Implementuje ove interfejse zbog prebacivanja izmedju aktivnosti putem intenta

    private String naziv;
    private String id;
    private String idIzFirebasea;

    public Kategorija() {
    }

    public Kategorija(String naziv, String id) {
        this.naziv = naziv;
        this.id = id;
    }

    public Kategorija(String naziv, String id, String idIzFirebasea) {
        this.naziv = naziv;
        this.id = id;
        this.idIzFirebasea = idIzFirebasea;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdIzFirebasea() {
        return idIzFirebasea;
    }

    public void setIdIzFirebasea(String idIzFirebasea) {
        this.idIzFirebasea = idIzFirebasea;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeString(id);
        dest.writeString(idIzFirebasea);
    }

    public static final Creator<Kategorija> CREATOR = new Creator<Kategorija>() {
        @Override
        public Kategorija createFromParcel(Parcel in) {
            return new Kategorija(in);
        }

        @Override
        public Kategorija[] newArray(int size) {
            return new Kategorija[size];
        }
    };

    protected Kategorija(Parcel in) {
        naziv = in.readString();
        id = in.readString();
        idIzFirebasea = in.readString();
    }

    // Ove dodatne metode su zbog interfejsa Parcelable
}
