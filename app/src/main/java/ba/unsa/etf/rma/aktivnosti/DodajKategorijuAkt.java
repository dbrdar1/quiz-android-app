package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;

import java.io.Serializable;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.broadcastreceiveri.InternetBroadcastReceiver;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.korisneklase.OsvjezivacPodatakaUBazama;
import ba.unsa.etf.rma.resultreceiveri.DodavanjeKategorijeReceiver;
import ba.unsa.etf.rma.resultreceiveri.ProvjeraKategorijePoNazivuReceiver;
import ba.unsa.etf.rma.servisi.DodavanjeKategorijeService;
import ba.unsa.etf.rma.servisi.ProvjeraKategorijePoNazivuService;

public class DodajKategorijuAkt extends AppCompatActivity implements IconDialog.Callback,
        ProvjeraKategorijePoNazivuReceiver.Receiver, DodavanjeKategorijeReceiver.Receiver,
        InternetBroadcastReceiver.Aktivnost {

    private Kategorija povratnaKategorija = new Kategorija();
    private ArrayList<Kategorija> listaDosadasnjihKategorija = new ArrayList<>();
    private boolean validnaForma;
    private Icon[] nizIkona;
    private EditText etNaziv;
    private EditText etIkona;
    private Button btnDodajIkonu;
    private Button btnDodajKategoriju;
    private float velicinaSlovaEditTexta;
    private int paddingEditTexta;
    private InternetBroadcastReceiver internetBroadcastReceiver = new InternetBroadcastReceiver();
    private IntentFilter internetIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    private boolean imaInternetKonekcije;
    private boolean prvo;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodaj_kategoriju_akt);
        progressDialog = new ProgressDialog(DodajKategorijuAkt.this);
        dobaviWidgete();
        dobaviDimenzijeEditTexta();
        pokupiPoslanePodatke();
        onemoguciEditTextIkona();
        postaviListenerNaDugmeDodajIkonu();
        postaviListenerNaDugmeDodajKategoriju();
    }

    private void dobaviWidgete() {
        etNaziv = (EditText) findViewById(R.id.etNaziv);
        etIkona = (EditText) findViewById(R.id.etIkona);
        btnDodajIkonu = (Button) findViewById(R.id.btnDodajIkonu);
        btnDodajKategoriju = (Button) findViewById(R.id.btnDodajKategoriju);
    }

    private void dobaviDimenzijeEditTexta() {
        paddingEditTexta = etNaziv.getPaddingBottom();
        velicinaSlovaEditTexta = etNaziv.getTextSize();
    }

    private void pokupiPoslanePodatke() {
        listaDosadasnjihKategorija = getIntent().getParcelableArrayListExtra("extraListaDosadasnjihKategorija");
    }

    private void onemoguciEditTextIkona() {
        etIkona.setFocusable(false);
    }

    private void postaviListenerNaDugmeDodajIkonu() {
        btnDodajIkonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pokreniIconDialog();
            }
        });
    }

    private void pokreniIconDialog() {
        IconDialog iconDialog = new IconDialog();
        iconDialog.setSelectedIcons(nizIkona);
        iconDialog.show(getSupportFragmentManager(), "icon_dialog");
    }

    private void postaviListenerNaDugmeDodajKategoriju() {
        btnDodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validirajFormu();
                provjeriPostojanjeKategorijeUFirestoreu(etNaziv.getText().toString());
            }
        });
    }

    private void validirajFormu() {
        validnaForma = true;
        boolean zacrvenitiEtNaziv = false;
        if (jeLiPrazanNazivKategorije()) {
            zacrvenitiEtNaziv = true;
            validnaForma = false;
        }
        if (etNaziv.getText().toString().trim().equals("Svi")) {
            zacrvenitiEtNaziv = true;
            validnaForma = false;
        }
        if (etNaziv.getText().toString().trim().equals("Dodaj kategoriju")) {
            zacrvenitiEtNaziv = true;
            validnaForma = false;
        }
        if (zacrvenitiEtNaziv) etNaziv.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
        else resetujPozadinuEtNaziv();
        boolean zacrvenitiEtIkona = false;
        if (jeLiPrazanIDIkone()) {
            zacrvenitiEtIkona = true;
            validnaForma = false;
        }
        if (zacrvenitiEtIkona) etIkona.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
        else resetujPozadinuEtIkona();
    }

    private boolean jeLiPrazanNazivKategorije() {
        return etNaziv.getText().toString().trim().length() == 0;
    }

    private void resetujPozadinuEtNaziv() {
        etNaziv.setBackgroundResource(android.R.drawable.editbox_background_normal);
        etNaziv.setTextSize(velicinaSlovaEditTexta / 3);
        etNaziv.setPadding(paddingEditTexta, paddingEditTexta, paddingEditTexta, paddingEditTexta);
    }

    private boolean jeLiPrazanIDIkone() {
        return etIkona.getText().toString().trim().length() == 0;
    }

    private void resetujPozadinuEtIkona() {
        etIkona.setBackgroundResource(android.R.drawable.editbox_background_normal);
        etIkona.setTextSize(velicinaSlovaEditTexta / 3);
        etIkona.setPadding(paddingEditTexta, paddingEditTexta, paddingEditTexta, paddingEditTexta);
    }

    private void provjeriPostojanjeKategorijeUFirestoreu(String nazivKategorije) {
        ProvjeraKategorijePoNazivuReceiver provjeraKategorijePoNazivuReceiver = new ProvjeraKategorijePoNazivuReceiver(new Handler());
        provjeraKategorijePoNazivuReceiver.setReceiver(this);
        Intent intent = new Intent(this, ProvjeraKategorijePoNazivuService.class);
        intent.putExtra("extraProvjeraKatPoNazivuReceiver", provjeraKategorijePoNazivuReceiver);
        intent.putExtra("extraNazivZadaneKategorije", nazivKategorije);
        startService(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intentPovratakNaKviz = new Intent();
        setResult(RESULT_CANCELED, intentPovratakNaKviz);
        finish();
    }

    @Override
    public void onIconDialogIconsSelected(Icon[] icons) {
        nizIkona = icons;

        // Automatsko upisivanje ID-ja ikone u EditText nakon sto je odabrana

        etIkona.setText(String.valueOf(icons[0].getId()));
    }

    @Override
    public void naSaznanjeOPostojanjuKategorijePoNazivu(int resultCode, Bundle resultData) {
        if (resultCode == ProvjeraKategorijePoNazivuService.KATEGORIJA_VEC_POSTOJI) {
            etNaziv.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
            prikaziAlertPostojecaKategorijaUBazi();
        }
        else if (resultCode == ProvjeraKategorijePoNazivuService.KATEGORIJA_NE_POSTOJI) {
            if (!jeLiPrazanNazivKategorije()) resetujPozadinuEtNaziv();
            if (!validnaForma) return;
            povratnaKategorija.setNaziv(etNaziv.getText().toString());
            povratnaKategorija.setId(etIkona.getText().toString());
            dodajNovuKategorijuUSQLite(povratnaKategorija);
            dodajNovuKategorijuUFirebase(povratnaKategorija);
        }
    }

    private void dodajNovuKategorijuUSQLite(Kategorija povratnaKategorija) {
        new OsvjezivacPodatakaUBazama(this).upisiNovuKategorijuLokalno(povratnaKategorija);
    }

    private void prikaziAlertPostojecaKategorijaUBazi() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Unesena kategorija već postoji!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void dodajNovuKategorijuUFirebase(Kategorija kategorija) {
        DodavanjeKategorijeReceiver dodavanjeKategorijeReceiver = new DodavanjeKategorijeReceiver((new Handler()));
        dodavanjeKategorijeReceiver.setReceiver(this);
        Intent intent = new Intent(this, DodavanjeKategorijeService.class);
        intent.putExtra("extraDodavanjeKategorijeReceiver", dodavanjeKategorijeReceiver);
        intent.putExtra("extraKategorijaZaDodavanje", (Serializable) kategorija);
        startService(intent);
    }

    @Override
    public void naSaznanjeODodanojKategoriji(int resultCode, Bundle resultData) {
        if (resultCode == DodavanjeKategorijeService.KATEGORIJA_DODANA) {
            String idDokumentaIzBaze = resultData.getString("extraVraceniIdKategorije");
            povratnaKategorija.setIdIzFirebasea(idDokumentaIzBaze);
            vratiSeUDodajKvizAkt();
        }
    }

    private void vratiSeUDodajKvizAkt() {
        Intent intentPovratakNaKviz = new Intent();
        intentPovratakNaKviz.putExtra("extraPovratnaKategorija", (Serializable) povratnaKategorija);
        setResult(RESULT_OK, intentPovratakNaKviz);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        prvo = true;
        internetBroadcastReceiver.setAktivnost(this);
        registerReceiver(internetBroadcastReceiver, internetIntentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(internetBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void izmijeniStatusKonekcije(boolean imaKonekcije) {
        imaInternetKonekcije = imaKonekcije;
        if (imaKonekcije) omoguciDodavanje();
        else onemoguciDodavanje();
        if (prvo) {
            prvo = false;
            return;
        }
        if (imaKonekcije) new OsvjezivacPodatakaUBazama(this).azurirajStanjeUSQLiteuIFirebaseu();
    }

    private void omoguciDodavanje() {
        if (!btnDodajKategoriju.isEnabled()) btnDodajKategoriju.setEnabled(true);
    }

    private void onemoguciDodavanje() {
        if (btnDodajKategoriju.isEnabled()) btnDodajKategoriju.setEnabled(false);
    }
}
