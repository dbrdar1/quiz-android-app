package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Pitanje;

public class UcitavanjePitanjaService extends IntentService {

    public static final int PITANJA_UCITANA = 0;
    private ArrayList<Pitanje> ucitanaPitanja = new ArrayList<>();

    public UcitavanjePitanjaService() {
        super(null);
    }

    public UcitavanjePitanjaService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("stat", "ucitavanjePitanjaPokrenuto");
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraUcitavanjePitanjaReceiver");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Pitanja?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                Log.d("ODGOVORPitanja", rezultujuciJsonString);
                JSONObject jsonObject = new JSONObject(rezultujuciJsonString);
                JSONArray dokumentiArray = new JSONArray();
                try {
                    dokumentiArray = jsonObject.getJSONArray("documents");
                } catch (JSONException e) {
                    bundle.putParcelableArrayList("extraUcitanaPitanja", ucitanaPitanja);
                    resultReceiver.send(PITANJA_UCITANA, bundle);
                    Log.d("status", "zavrsilo ucitavanje pitanja");
                    return;
                }
                for (int i = 0; i < dokumentiArray.length(); i++) {
                    JSONObject pojedinacniDokumentObject = dokumentiArray.getJSONObject(i);
                    String spojeniNizString = pojedinacniDokumentObject.getString("name");
                    String[] nizString = spojeniNizString.split("/");
                    int duzinaNizStringa = nizString.length;
                    String idDokumentaPitanjaString = nizString[duzinaNizStringa - 1];
                    Log.d("IdDokServis", idDokumentaPitanjaString);
                    JSONObject poljaObject = pojedinacniDokumentObject.getJSONObject("fields");
                    JSONObject nazivObject = poljaObject.getJSONObject("naziv");
                    JSONObject indexTacnogObject = poljaObject.getJSONObject("indexTacnog");
                    JSONObject odgovoriObject = poljaObject.getJSONObject("odgovori");
                    String nazivString = nazivObject.getString("stringValue");
                    String indexTacnogString = indexTacnogObject.getString("integerValue");
                    JSONObject odgovoriArrayValue = odgovoriObject.getJSONObject("arrayValue");
                    JSONArray vrijednostiArray = odgovoriArrayValue.getJSONArray("values");
                    String tacanOdgovor = "";
                    ArrayList<String> ucitaniOdgovoriPitanja = new ArrayList<>();
                    for (int brojac = 0; brojac < vrijednostiArray.length(); brojac++) {
                        JSONObject pojedinacniOdgovorObject = vrijednostiArray.getJSONObject(brojac);
                        String pojedinacniOdgovorString = pojedinacniOdgovorObject.getString("stringValue");
                        if (String.valueOf(brojac).equals(indexTacnogString)) tacanOdgovor = pojedinacniOdgovorString;
                        ucitaniOdgovoriPitanja.add(pojedinacniOdgovorString);
                    }
                    Pitanje povratno = new Pitanje();
                    povratno.setNaziv(nazivString);
                    povratno.setTekstPitanja(nazivString);
                    povratno.setOdgovori(ucitaniOdgovoriPitanja);
                    povratno.setTacan(tacanOdgovor);
                    povratno.setIdIzFirebasea(idDokumentaPitanjaString);
                    ucitanaPitanja.add(povratno);
                }
                bundle.putParcelableArrayList("extraUcitanaPitanja", ucitanaPitanja);
                resultReceiver.send(PITANJA_UCITANA, bundle);
                Log.d("status", "zavrsilo ucitavanje pitanja");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
