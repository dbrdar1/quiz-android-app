package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class UcitavanjeKvizovaPoKategorijiService extends IntentService {

    public static final int KVIZOVI_UCITANI_PO_KATEGORIJI = 0;
    private ArrayList<Kviz> ucitaniKvizovi = new ArrayList<>();
    private String idZadaneKategorijeIzFirebasea = "";

    public UcitavanjeKvizovaPoKategorijiService() {
        super(null);
    }

    public UcitavanjeKvizovaPoKategorijiService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("stat", "ucitaPoKatPokrenuto");
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraUcitavanjeKvizovaPoKategorijiReceiver");
        idZadaneKategorijeIzFirebasea = intent.getStringExtra("extraIdZadaneKategorijeIzFirebasea");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents:runQuery?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            JSONObject jsonObject = new JSONObject();
            JSONObject structuredQueryObject = new JSONObject();
            JSONObject whereObject = new JSONObject();
            JSONObject selectObject = new JSONObject();
            JSONArray fromArray = new JSONArray();
            JSONObject fieldFilterObject = new JSONObject();
            JSONObject fieldObject = new JSONObject();
            JSONObject valueOject = new JSONObject();
            JSONArray fieldsArray = new JSONArray();
            JSONObject pojedinacniNazivObject = new JSONObject();
            JSONObject pojedinacniPitanjaObject = new JSONObject();
            JSONObject pojedinacniCollectionIdObject = new JSONObject();
            fieldObject.put("fieldPath", "idKategorije");
            valueOject.put("stringValue", idZadaneKategorijeIzFirebasea);
            fieldFilterObject.put("field", fieldObject);
            fieldFilterObject.put("op", "EQUAL");
            fieldFilterObject.put("value", valueOject);
            whereObject.put("fieldFilter", fieldFilterObject);
            pojedinacniNazivObject.put("fieldPath", "naziv");
            pojedinacniPitanjaObject.put("fieldPath", "pitanja");
            fieldsArray.put(pojedinacniNazivObject);
            fieldsArray.put(pojedinacniPitanjaObject);
            selectObject.put("fields", fieldsArray);
            pojedinacniCollectionIdObject.put("collectionId", "Kvizovi");
            fromArray.put(pojedinacniCollectionIdObject);
            structuredQueryObject.put("where", whereObject);
            structuredQueryObject.put("select", selectObject);
            structuredQueryObject.put("from", fromArray);
            jsonObject.put("structuredQuery", structuredQueryObject);
            String strukturiraniUpit = jsonObject.toString();
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = strukturiraniUpit.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                rezultujuciJsonString = "{\"documents\": " + rezultujuciJsonString + "}";
                Log.d("ODGOVORKvizoviPoKa", rezultujuciJsonString);
                JSONObject jsonObjectDobijeni = new JSONObject(rezultujuciJsonString);
                JSONArray dokumentiArray = jsonObjectDobijeni.getJSONArray("documents");
                for (int i = 0; i < dokumentiArray.length(); i++) {
                    JSONObject pojedinacniDokumentObject = dokumentiArray.getJSONObject(i);
                    if (!pojedinacniDokumentObject.has("document")) {
                        break;
                    }
                    JSONObject documentObject = pojedinacniDokumentObject.getJSONObject("document");
                    String spojeniNizString = documentObject.getString("name");
                    String[] nizString = spojeniNizString.split("/");
                    int duzinaNizStringa = nizString.length;
                    String idDokumentaKvizaString = nizString[duzinaNizStringa - 1];
                    JSONObject poljaObject = documentObject.getJSONObject("fields");
                    JSONObject nazivObject = poljaObject.getJSONObject("naziv");
                    JSONObject pitanjaObject = poljaObject.getJSONObject("pitanja");
                    JSONObject pitanjaArrayValue = pitanjaObject.getJSONObject("arrayValue");
                    JSONArray vrijednostiArray = new JSONArray();
                    ArrayList<String> listaUcitanihIdjevaPitanja = new ArrayList<>();
                    try {
                        vrijednostiArray = pitanjaArrayValue.getJSONArray("values");
                        for (int brojac = 0; brojac < vrijednostiArray.length(); brojac++) {
                            JSONObject pojedinacniIdObject = vrijednostiArray.getJSONObject(brojac);
                            String pojedinacniIdString = pojedinacniIdObject.getString("stringValue");
                            listaUcitanihIdjevaPitanja.add(pojedinacniIdString);
                        }
                    } catch (JSONException e) {

                    }
                    String nazivString = nazivObject.getString("stringValue");
                    Kategorija ucitanaKategorijaKviza = new Kategorija();
                    if (!idZadaneKategorijeIzFirebasea.equals("ktgrjSvi")) ucitanaKategorijaKviza = ucitajIDajKategorijuPoIdjuDokumenta(idZadaneKategorijeIzFirebasea);
                    else ucitanaKategorijaKviza = new Kategorija("Svi", "976", "ktgrjSvi");
                    ArrayList<Pitanje> ucitanaPitanjaKviza = ucitajIDajPitanjaPoIdjevimaDokumenata(listaUcitanihIdjevaPitanja);
                    ucitaniKvizovi.add(new Kviz(nazivString, ucitanaPitanjaKviza, ucitanaKategorijaKviza, idDokumentaKvizaString));
                }
                bundle.putParcelableArrayList("extraUcitaniKvizoviPoKategoriji", ucitaniKvizovi);
                resultReceiver.send(KVIZOVI_UCITANI_PO_KATEGORIJI, bundle);
                Log.d("status", "zavrsio servis po kategoriji");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Kategorija ucitajIDajKategorijuPoIdjuDokumenta(String idDokumenta) {
        Kategorija povratna = new Kategorija();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Kategorije/" +
                    URLEncoder.encode(idDokumenta, "UTF-8") + "?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                Log.d("ODGOVORKategorija", rezultujuciJsonString);
                JSONObject jsonObject = new JSONObject(rezultujuciJsonString);
                JSONObject poljaObject = jsonObject.getJSONObject("fields");
                JSONObject nazivObject = poljaObject.getJSONObject("naziv");
                JSONObject idIkoniceObject = poljaObject.getJSONObject("idIkonice");
                String nazivString = nazivObject.getString("stringValue");
                String idIkoniceString = idIkoniceObject.getString("integerValue");
                povratna.setNaziv(nazivString);
                povratna.setId(idIkoniceString);
                povratna.setIdIzFirebasea(idDokumenta);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return povratna;
    }

    private ArrayList<Pitanje> ucitajIDajPitanjaPoIdjevimaDokumenata(ArrayList<String> idjeviDokumenata) {
        ArrayList<Pitanje> povratna = new ArrayList<>();
        if (idjeviDokumenata.size() == 0) return povratna;
        for (String pojedinacniId : idjeviDokumenata) {
            Pitanje pojedinacno = ucitajIDajPitanjePoIdjuDokumenta(pojedinacniId);
            povratna.add(pojedinacno);
        }
        return povratna;
    }

    private Pitanje ucitajIDajPitanjePoIdjuDokumenta(String idDokumenta) {
        Pitanje povratno = new Pitanje();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Pitanja/" +
                    URLEncoder.encode(idDokumenta, "UTF-8") + "?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                Log.d("ODGOVORPitanje", rezultujuciJsonString);
                JSONObject jsonObject = new JSONObject(rezultujuciJsonString);
                JSONObject poljaObject = jsonObject.getJSONObject("fields");
                JSONObject nazivObject = poljaObject.getJSONObject("naziv");
                JSONObject indexTacnogObject = poljaObject.getJSONObject("indexTacnog");
                JSONObject odgovoriObject = poljaObject.getJSONObject("odgovori");
                String nazivString = nazivObject.getString("stringValue");
                String indexTacnogString = indexTacnogObject.getString("integerValue");
                JSONObject odgovoriArrayValue = odgovoriObject.getJSONObject("arrayValue");
                JSONArray vrijednostiArray = odgovoriArrayValue.getJSONArray("values");
                String tacanOdgovor = "";
                ArrayList<String> ucitaniOdgovoriPitanja = new ArrayList<>();
                for (int brojac = 0; brojac < vrijednostiArray.length(); brojac++) {
                    JSONObject pojedinacniOdgovorObject = vrijednostiArray.getJSONObject(brojac);
                    String pojedinacniOdgovorString = pojedinacniOdgovorObject.getString("stringValue");
                    if (String.valueOf(brojac).equals(indexTacnogString)) tacanOdgovor = pojedinacniOdgovorString;
                    ucitaniOdgovoriPitanja.add(pojedinacniOdgovorString);
                }
                povratno.setNaziv(nazivString);
                povratno.setTekstPitanja(nazivString);
                povratno.setOdgovori(ucitaniOdgovoriPitanja);
                povratno.setTacan(tacanOdgovor);
                povratno.setIdIzFirebasea(idDokumenta);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return povratno;
    }
}
