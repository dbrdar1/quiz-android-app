package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.broadcastreceiveri.InternetBroadcastReceiver;
import ba.unsa.etf.rma.klase.OdgovoriAdapter;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.korisneklase.OsvjezivacPodatakaUBazama;
import ba.unsa.etf.rma.resultreceiveri.DodavanjePitanjaReceiver;
import ba.unsa.etf.rma.resultreceiveri.ProvjeraPitanjaPoNazivuReceiver;
import ba.unsa.etf.rma.servisi.DodavanjePitanjaService;
import ba.unsa.etf.rma.servisi.ProvjeraPitanjaPoNazivuService;

public class DodajPitanjeAkt extends AppCompatActivity implements ProvjeraPitanjaPoNazivuReceiver.Receiver,
        DodavanjePitanjaReceiver.Receiver, InternetBroadcastReceiver.Aktivnost {

    private ArrayList<String> listaOdgovora = new ArrayList<>();
    private ArrayList<Pitanje> listaDosadasnjihDodanihPitanja = new ArrayList<>();
    private ArrayList<Pitanje> listaDosadasnjihMogucihPitanja = new ArrayList<>();
    private Pitanje povratnoPitanje = new Pitanje();
    private String tacanOdgovor = "";
    private boolean validnaForma;
    private boolean validnoDodavanjedOdgovora;
    private EditText etNaziv;
    private EditText etOdgovor;
    private Button btnDodajOdgovor;
    private Button btnDodajTacan;
    private Button btnDodajPitanje;
    private ListView lvOdgovori;
    private float velicinaSlovaEditTexta;
    private int paddingEditTexta;
    private OdgovoriAdapter odgovoriAdapter;
    private InternetBroadcastReceiver internetBroadcastReceiver = new InternetBroadcastReceiver();
    private IntentFilter internetIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    private boolean imaInternetKonekcije;
    private boolean prvo;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodaj_pitanje_akt);
        progressDialog = new ProgressDialog(DodajPitanjeAkt.this);
        dobaviWidgete();
        dobaviDimenzijeEditTexta();
        pokupiPoslanePodatke();
        postaviAdaptere();
        postaviListenerZaSkrolanjeListeOdgovora();
        postaviListenerNaDugmeDodavanjeTacnog();
        postaviListenerNaDugmeDodavanjeOdgovora();
        postaviListenerNaDugmeDodavanjePitanja();
        postaviListenerNaOdgovor();
    }

    private void dobaviWidgete() {
        etNaziv = (EditText) findViewById(R.id.etNaziv);
        etOdgovor = (EditText) findViewById(R.id.etOdgovor);
        btnDodajOdgovor = (Button) findViewById(R.id.btnDodajOdgovor);
        btnDodajTacan = (Button) findViewById(R.id.btnDodajTacan);
        btnDodajPitanje = (Button) findViewById(R.id.btnDodajPitanje);
        lvOdgovori = (ListView) findViewById(R.id.lvOdgovori);
    }

    private void dobaviDimenzijeEditTexta() {
        paddingEditTexta = etNaziv.getPaddingBottom();
        velicinaSlovaEditTexta = etNaziv.getTextSize();
    }

    private void pokupiPoslanePodatke() {
        listaDosadasnjihDodanihPitanja = getIntent().getParcelableArrayListExtra("extraListaDosadasnjihDodanihPitanja");
        listaDosadasnjihMogucihPitanja = getIntent().getParcelableArrayListExtra("extraListaDosadasnjihMogucihPitanja");
    }

    private void postaviAdaptere() {
        odgovoriAdapter = new OdgovoriAdapter(this, listaOdgovora, getResources());
        odgovoriAdapter.setTacanOdgovor(tacanOdgovor);
        lvOdgovori.setAdapter(odgovoriAdapter);
    }

    private void postaviListenerZaSkrolanjeListeOdgovora() {
        lvOdgovori.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    private void postaviListenerNaOdgovor() {
        lvOdgovori.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listaOdgovora.size() == 0) return;
                if (tacanOdgovor.equals(listaOdgovora.get(position))) {
                    tacanOdgovor = "";
                    btnDodajTacan.setEnabled(true);
                }
                obrisiOdgovorIzListe(position);
                osvjeziPrikazOdgovora();
            }
        });
    }

    private void obrisiOdgovorIzListe(int pozicijaUListi) {
        listaOdgovora.remove(pozicijaUListi);
    }

    private void postaviListenerNaDugmeDodavanjeOdgovora() {
        btnDodajOdgovor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validirajDodavanjeOdgovora();
                if (!validnoDodavanjedOdgovora) return;
                dodajOdgovorUListu();
                osvjeziPrikazOdgovora();
                resetujEditTextOdgovor();
            }
        });
    }

    private void validirajDodavanjeOdgovora() {
        validnoDodavanjedOdgovora = true;
        boolean zacrvenitiEtOdgovor = false;
        if (jeLiPrazanOdgovor()) {
            zacrvenitiEtOdgovor = true;
            validnoDodavanjedOdgovora = false;
        }
        for (String odgovor : listaOdgovora) {
            if (etOdgovor.getText().toString().equals(odgovor)) {
                zacrvenitiEtOdgovor = true;
                validnoDodavanjedOdgovora = false;
                break;
            }
        }
        if (zacrvenitiEtOdgovor) etOdgovor.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
        else resetujPozadinuEtOdgovor();
    }

    private boolean jeLiPrazanOdgovor() {
        return etOdgovor.getText().toString().trim().length() == 0;
    }

    private void dodajOdgovorUListu() {
        listaOdgovora.add(etOdgovor.getText().toString());
    }

    private void osvjeziPrikazOdgovora() {
        odgovoriAdapter.setTacanOdgovor(tacanOdgovor);
        odgovoriAdapter.notifyDataSetChanged();
    }

    private void resetujEditTextOdgovor() {
        etOdgovor.setText("");
    }

    private void postaviListenerNaDugmeDodavanjeTacnog() {
        btnDodajTacan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validirajDodavanjeOdgovora();
                if (!validnoDodavanjedOdgovora) return;
                dodajOdgovorUListu();
                postaviTacanOdgovor(etOdgovor.getText().toString());
                btnDodajTacan.setEnabled(false);

                // Onemoguceno je dodavanje vise od jednog tacnog odgovora

                osvjeziPrikazOdgovora();
                resetujEditTextOdgovor();
            }
        });
    }

    private void postaviTacanOdgovor(String odgovor) {
        tacanOdgovor = odgovor;
        povratnoPitanje.setTacan(tacanOdgovor);
    }

    private void postaviListenerNaDugmeDodavanjePitanja() {
        btnDodajPitanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validirajFormu();
                provjeriPostojanjePitanjaUFirestoreu(etNaziv.getText().toString());
            }
        });
    }

    private void validirajFormu() {
        validnaForma = true;
        boolean zacrvenitiEtNaziv = false;
        if (jeLiPrazanNazivPitanja()) {
            zacrvenitiEtNaziv = true;
            validnaForma = false;
        }
        if (zacrvenitiEtNaziv) etNaziv.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
        else resetujPozadinuEtNaziv();
        boolean zacrvenitiEtOdgovor = false;
        if (listaOdgovora.size() == 0) {
            zacrvenitiEtOdgovor = true;
            validnaForma = false;
        }
        if (tacanOdgovor.equals("")) {
            zacrvenitiEtOdgovor = true;
            validnaForma = false;
        }
        if (btnDodajTacan.isEnabled()) {
            zacrvenitiEtOdgovor = true;
            validnaForma = false;
        }
        if (zacrvenitiEtOdgovor) etOdgovor.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
        else resetujPozadinuEtOdgovor();
    }

    private boolean jeLiPrazanNazivPitanja() {
        return etNaziv.getText().toString().trim().length() == 0;
    }

    private void resetujPozadinuEtNaziv() {
        etNaziv.setBackgroundResource(android.R.drawable.editbox_background_normal);
        etNaziv.setTextSize(velicinaSlovaEditTexta / 3);
        etNaziv.setPadding(paddingEditTexta, paddingEditTexta, paddingEditTexta, paddingEditTexta);
    }

    private void resetujPozadinuEtOdgovor() {
        etOdgovor.setBackgroundResource(android.R.drawable.editbox_background_normal);
        etOdgovor.setTextSize(velicinaSlovaEditTexta / 3);
        etOdgovor.setPadding(paddingEditTexta, paddingEditTexta, paddingEditTexta, paddingEditTexta);
    }

    private void provjeriPostojanjePitanjaUFirestoreu(String nazivPitanja) {
        ProvjeraPitanjaPoNazivuReceiver provjeraPitanjaPoNazivuReceiver = new ProvjeraPitanjaPoNazivuReceiver(new Handler());
        provjeraPitanjaPoNazivuReceiver.setReceiver(this);
        Intent intent = new Intent(this, ProvjeraPitanjaPoNazivuService.class);
        intent.putExtra("extraProvjeraPitPoNazivuReceiver", provjeraPitanjaPoNazivuReceiver);
        intent.putExtra("extraNazivZadanogPitanja", nazivPitanja);
        startService(intent);
    }

    @Override
    public void naSaznanjeOPostojanjuPitanjaPoNazivu(int resultCode, Bundle resultData) {
        if (resultCode == ProvjeraPitanjaPoNazivuService.PITANJE_VEC_POSTOJI) {
            etNaziv.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
            prikaziAlertPostojecePitanjeUBazi();
        }
        else if (resultCode == ProvjeraPitanjaPoNazivuService.PITANJE_NE_POSTOJI) {
            if (!jeLiPrazanNazivPitanja()) resetujPozadinuEtNaziv();
            if (!validnaForma) return;
            povratnoPitanje.setNaziv(etNaziv.getText().toString());
            povratnoPitanje.setTekstPitanja(etNaziv.getText().toString());
            povratnoPitanje.setOdgovori(listaOdgovora);
            povratnoPitanje.setTacan(tacanOdgovor);
            dodajNovoPitanjeUSQLite(povratnoPitanje);
            dodajNovoPitanjeUFirebase(povratnoPitanje);
        }
    }

    private void dodajNovoPitanjeUSQLite(Pitanje povratnoPitanje) {
        new OsvjezivacPodatakaUBazama(this).upisiNovoPitanjeLokalno(povratnoPitanje);
    }

    private void prikaziAlertPostojecePitanjeUBazi() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Uneseno pitanje već postoji!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void dodajNovoPitanjeUFirebase(Pitanje pitanje) {
        DodavanjePitanjaReceiver dodavanjePitanjaReceiver = new DodavanjePitanjaReceiver(new Handler());
        dodavanjePitanjaReceiver.setReceiver(this);
        Intent intent = new Intent(this, DodavanjePitanjaService.class);
        intent.putExtra("extraDodavanjePitanjaReceiver", dodavanjePitanjaReceiver);
        intent.putExtra("extraPitanjeZaDodavanje", (Serializable) pitanje);
        startService(intent);
    }

    @Override
    public void naSaznanjeODodanomPitanju(int resultCode, Bundle resultData) {
        if (resultCode == DodavanjePitanjaService.PITANJE_DODANO) {
            String idDokumentaIzBaze = resultData.getString("extraVraceniIdPitanja");
            povratnoPitanje.setIdIzFirebasea(idDokumentaIzBaze);
            vratiSeUDodajKvizAkt();
        }
    }

    private void vratiSeUDodajKvizAkt() {
        Intent intentPovratakNaKviz = new Intent();
        intentPovratakNaKviz.putExtra("extraPovratnoPitanje", (Serializable) povratnoPitanje);
        setResult(RESULT_OK, intentPovratakNaKviz);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        prvo = true;
        internetBroadcastReceiver.setAktivnost(this);
        registerReceiver(internetBroadcastReceiver, internetIntentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(internetBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void izmijeniStatusKonekcije(boolean imaKonekcije) {
        imaInternetKonekcije = imaKonekcije;
        if (imaKonekcije) omoguciDodavanje();
        else onemoguciDodavanje();
        if (prvo) {
            prvo = false;
            return;
        }
        if (imaKonekcije) new OsvjezivacPodatakaUBazama(this).azurirajStanjeUSQLiteuIFirebaseu();
    }

    private void omoguciDodavanje() {
        if (!btnDodajPitanje.isEnabled()) btnDodajPitanje.setEnabled(true);
    }

    private void onemoguciDodavanje() {
        if (btnDodajPitanje.isEnabled()) btnDodajPitanje.setEnabled(false);
    }
}
