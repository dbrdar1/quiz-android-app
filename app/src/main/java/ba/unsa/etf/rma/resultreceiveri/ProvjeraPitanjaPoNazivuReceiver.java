package ba.unsa.etf.rma.resultreceiveri;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

public class ProvjeraPitanjaPoNazivuReceiver extends ResultReceiver {

    private Receiver receiver;

    public ProvjeraPitanjaPoNazivuReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public interface Receiver {
        void naSaznanjeOPostojanjuPitanjaPoNazivu(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        Log.d("status", "primio rezultat");
        if (receiver != null) receiver.naSaznanjeOPostojanjuPitanjaPoNazivu(resultCode, resultData);
    }
}
