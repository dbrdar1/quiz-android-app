package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class UcitavanjeKvizaPoIdjuService extends IntentService {

    public static final int KVIZ_UCITAN = 0;
    Kviz ucitaniKviz = new Kviz();
    String zadaniIdDokumentaKviza = "";
    private ArrayList<Pitanje> listaSvihUcitanihPitanja = new ArrayList<>();
    private ArrayList<Pitanje> ucitanaPitanjaKviza = new ArrayList<>();
    private ArrayList<Pitanje> ucitanaMogucaPitanja = new ArrayList<>();

    public UcitavanjeKvizaPoIdjuService() {
        super(null);
    }

    public UcitavanjeKvizaPoIdjuService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("stat", "ucitaPoIdPokrenuto");
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraUcitavanjeKvizaPoIdjuReceiver");
        zadaniIdDokumentaKviza = intent.getStringExtra("extraZadaniIdDokumentaKviza");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Kvizovi/" +
                    URLEncoder.encode(zadaniIdDokumentaKviza, "UTF-8") + "?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                Log.d("ODGOVORKviz", rezultujuciJsonString);
                JSONObject jsonObject = new JSONObject(rezultujuciJsonString);
                String spojeniNizString = jsonObject.getString("name");
                String[] nizString = spojeniNizString.split("/");
                int duzinaNizStringa = nizString.length;
                String idDokumentaKvizaString = nizString[duzinaNizStringa - 1];
                Log.d("IdDokServis", idDokumentaKvizaString);
                JSONObject poljaObject = jsonObject.getJSONObject("fields");
                JSONObject nazivObject = poljaObject.getJSONObject("naziv");
                JSONObject idKategorijeObject = poljaObject.getJSONObject("idKategorije");
                JSONObject pitanjaObject = poljaObject.getJSONObject("pitanja");
                JSONObject pitanjaArrayValue = pitanjaObject.getJSONObject("arrayValue");
                JSONArray vrijednostiArray = new JSONArray();
                ArrayList<String> listaUcitanihIdjevaPitanja = new ArrayList<>();
                try {
                    vrijednostiArray = pitanjaArrayValue.getJSONArray("values");
                    for (int brojac = 0; brojac < vrijednostiArray.length(); brojac++) {
                        JSONObject pojedinacniIdObject = vrijednostiArray.getJSONObject(brojac);
                        String pojedinacniIdString = pojedinacniIdObject.getString("stringValue");
                        listaUcitanihIdjevaPitanja.add(pojedinacniIdString);
                    }
                } catch (JSONException e) {

                }
                String nazivString = nazivObject.getString("stringValue");
                String idKategorijeString = idKategorijeObject.getString("stringValue");
                Kategorija ucitanaKategorijaKviza = new Kategorija();
                if (!idKategorijeString.equals("ktgrjSvi")) ucitanaKategorijaKviza = ucitajIDajKategorijuPoIdjuDokumenta(idKategorijeString);
                else ucitanaKategorijaKviza = new Kategorija("Svi", "976", "ktgrjSvi");
                ucitajSvaPitanja();
                podijeliPitanjaPoIdjevima(listaUcitanihIdjevaPitanja);
                ucitaniKviz = new Kviz(nazivString, ucitanaPitanjaKviza, ucitanaKategorijaKviza, idDokumentaKvizaString);
                bundle.putSerializable("extraUcitaniKvizPoIdju", ucitaniKviz);
                bundle.putParcelableArrayList("extraOstalaPitanja", ucitanaMogucaPitanja);
                resultReceiver.send(KVIZ_UCITAN, bundle);
                Log.d("status", "zavrsio servis");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Kategorija ucitajIDajKategorijuPoIdjuDokumenta(String idDokumenta) {
        Kategorija povratna = new Kategorija();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Kategorije/" +
                    URLEncoder.encode(idDokumenta, "UTF-8") + "?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                Log.d("ODGOVORKategorija", rezultujuciJsonString);
                JSONObject jsonObject = new JSONObject(rezultujuciJsonString);
                JSONObject poljaObject = jsonObject.getJSONObject("fields");
                JSONObject nazivObject = poljaObject.getJSONObject("naziv");
                JSONObject idIkoniceObject = poljaObject.getJSONObject("idIkonice");
                String nazivString = nazivObject.getString("stringValue");
                String idIkoniceString = idIkoniceObject.getString("integerValue");
                povratna.setNaziv(nazivString);
                povratna.setId(idIkoniceString);
                povratna.setIdIzFirebasea(idDokumenta);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return povratna;
    }

    private void ucitajSvaPitanja() {
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Pitanja?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                Log.d("ODGOVORPitanja", rezultujuciJsonString);
                JSONObject jsonObject = new JSONObject(rezultujuciJsonString);
                JSONArray dokumentiArray = jsonObject.getJSONArray("documents");
                for (int i = 0; i < dokumentiArray.length(); i++) {
                    JSONObject pojedinacniDokumentObject = dokumentiArray.getJSONObject(i);
                    String spojeniNizString = pojedinacniDokumentObject.getString("name");
                    String[] nizString = spojeniNizString.split("/");
                    int duzinaNizStringa = nizString.length;
                    String idDokumentaPitanjaString = nizString[duzinaNizStringa - 1];
                    Log.d("IdDokServis", idDokumentaPitanjaString);
                    JSONObject poljaObject = pojedinacniDokumentObject.getJSONObject("fields");
                    JSONObject nazivObject = poljaObject.getJSONObject("naziv");
                    JSONObject indexTacnogObject = poljaObject.getJSONObject("indexTacnog");
                    JSONObject odgovoriObject = poljaObject.getJSONObject("odgovori");
                    String nazivString = nazivObject.getString("stringValue");
                    String indexTacnogString = indexTacnogObject.getString("integerValue");
                    JSONObject odgovoriArrayValue = odgovoriObject.getJSONObject("arrayValue");
                    JSONArray vrijednostiArray = odgovoriArrayValue.getJSONArray("values");
                    String tacanOdgovor = "";
                    ArrayList<String> ucitaniOdgovoriPitanja = new ArrayList<>();
                    for (int brojac = 0; brojac < vrijednostiArray.length(); brojac++) {
                        JSONObject pojedinacniOdgovorObject = vrijednostiArray.getJSONObject(brojac);
                        String pojedinacniOdgovorString = pojedinacniOdgovorObject.getString("stringValue");
                        if (String.valueOf(brojac).equals(indexTacnogString)) tacanOdgovor = pojedinacniOdgovorString;
                        ucitaniOdgovoriPitanja.add(pojedinacniOdgovorString);
                    }
                    Pitanje povratno = new Pitanje();
                    povratno.setNaziv(nazivString);
                    povratno.setTekstPitanja(nazivString);
                    povratno.setOdgovori(ucitaniOdgovoriPitanja);
                    povratno.setTacan(tacanOdgovor);
                    povratno.setIdIzFirebasea(idDokumentaPitanjaString);
                    listaSvihUcitanihPitanja.add(povratno);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void podijeliPitanjaPoIdjevima(ArrayList<String> idjeviDokumenata) {
        for (Pitanje pitanje : listaSvihUcitanihPitanja) {
            if (jeLiIdPitanjaUListi(pitanje.getIdIzFirebasea(), idjeviDokumenata)) ucitanaPitanjaKviza.add(pitanje);
            else ucitanaMogucaPitanja.add(pitanje);
        }
    }

    private boolean jeLiIdPitanjaUListi(String zadaniId, ArrayList<String> listaIdjeva) {
        if (listaIdjeva.size() == 0) return false;
        for (String idIzListe : listaIdjeva) {
            if (zadaniId.equals(idIzListe)) return true;
        }
        return false;
    }
}
