package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class RangListaObjekat implements Parcelable, Serializable {

    // Implementuje ove interfejse zbog prebacivanja putem intenta

    private String nazivKviza;
    private ArrayList<ElementRangListe> lista = new ArrayList<>();
    private String idIzFirebasea;

    public RangListaObjekat() {
        nazivKviza = "";
    }

    public RangListaObjekat(String nazivKviza, ArrayList<ElementRangListe> lista) {
        this.nazivKviza = nazivKviza;
        this.lista = lista;
    }

    public RangListaObjekat(String nazivKviza, ArrayList<ElementRangListe> lista, String idIzFirebasea) {
        this.nazivKviza = nazivKviza;
        this.lista = lista;
        this.idIzFirebasea = idIzFirebasea;
    }

    public String getNazivKviza() {
        return nazivKviza;
    }

    public void setNazivKviza(String nazivKviza) {
        this.nazivKviza = nazivKviza;
    }

    public ArrayList<ElementRangListe> getLista() {
        return lista;
    }

    public void setLista(ArrayList<ElementRangListe> lista) {
        this.lista = lista;
    }

    public String getIdIzFirebasea() {
        return idIzFirebasea;
    }

    public void setIdIzFirebasea(String idIzFirebasea) {
        this.idIzFirebasea = idIzFirebasea;
    }

    public void dodajNoviElementUListu(ElementRangListe elementRangListe) {
        lista.add(elementRangListe);
    }

    public void sortirajListu() {
        Set<ElementRangListe> skupZaSortiranje = new TreeSet<>();
        skupZaSortiranje.addAll(lista);
        lista = new ArrayList<>();
        lista.addAll(skupZaSortiranje);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nazivKviza);
        dest.writeTypedList(lista);
        dest.writeString(idIzFirebasea);
    }

    public static final Parcelable.Creator<RangListaObjekat> CREATOR = new Parcelable.Creator<RangListaObjekat>() {
        @Override
        public RangListaObjekat createFromParcel(Parcel in) {
            return new RangListaObjekat(in);
        }

        @Override
        public RangListaObjekat[] newArray(int size) {
            return new RangListaObjekat[size];
        }
    };

    protected RangListaObjekat(Parcel in) {
        nazivKviza = in.readString();
        lista = in.createTypedArrayList(ElementRangListe.CREATOR);
        idIzFirebasea = in.readString();
    }

    // Ove dodatne metode su zbog interfejsa Parcelable
}
