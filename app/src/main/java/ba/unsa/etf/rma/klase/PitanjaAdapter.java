package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class PitanjaAdapter extends BaseAdapter {

    private Activity aktivnost;
    private ArrayList<Pitanje> listaPitanja;
    private static LayoutInflater layoutInflater = null;
    private Resources resursi;

    public PitanjaAdapter(Activity aktivnost, ArrayList<Pitanje> listaPitanja, Resources resursi) {
        this.aktivnost = aktivnost;
        this.listaPitanja = listaPitanja;
        this.resursi = resursi;
        layoutInflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaPitanja.size();
    }

    @Override
    public Pitanje getItem(int position) {
        return listaPitanja.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) convertView = layoutInflater.inflate(R.layout.element_liste_pitanja, null);
        TextView textView = (TextView) convertView.findViewById(R.id.tvNazivPitanja);
        textView.setText(listaPitanja.get(position).getNaziv());
        return convertView;
    }
}
