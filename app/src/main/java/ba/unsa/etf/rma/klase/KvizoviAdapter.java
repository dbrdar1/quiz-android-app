package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class KvizoviAdapter extends BaseAdapter {

    private Activity aktivnost;
    private ArrayList<Kviz> listaKvizova;
    private static LayoutInflater layoutInflater = null;
    private Resources resursi;

    public KvizoviAdapter(Activity aktivnost, ArrayList<Kviz> listaKvizova, Resources resursi) {
        this.aktivnost = aktivnost;
        this.listaKvizova = listaKvizova;
        this.resursi = resursi;
        layoutInflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaKvizova.size();
    }

    @Override
    public Kviz getItem(int position) {
        return listaKvizova.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) convertView = layoutInflater.inflate(R.layout.element_liste_kvizova, null);
        IconView iconView = (IconView) convertView.findViewById(R.id.icIkonaKategorije);
        iconView.setIcon(Integer.parseInt(listaKvizova.get(position).getKategorija().getId()));
        TextView textView = (TextView) convertView.findViewById(R.id.tvNazivKviza);
        textView.setText(listaKvizova.get(position).getNaziv());
        return convertView;
    }
}
