package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

    public class KategorijeAdapter extends BaseAdapter {

    private Activity aktivnost;
    private ArrayList<Kategorija> listaKategorija;
    private static LayoutInflater layoutInflater = null;
    private Resources resursi;

    public KategorijeAdapter(Activity aktivnost, ArrayList<Kategorija> listaKategorija, Resources resursi) {
        this.aktivnost = aktivnost;
        this.listaKategorija = listaKategorija;
        this.resursi = resursi;
        layoutInflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaKategorija.size();
    }

    @Override
    public Kategorija getItem(int position) {
        return listaKategorija.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) convertView = layoutInflater.inflate(R.layout.element_spinnera, null);
        TextView textView = (TextView) convertView.findViewById(R.id.tvNazivKategorije);
        textView.setText(listaKategorija.get(position).getNaziv());
        return convertView;
    }
}
